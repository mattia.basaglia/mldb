#!/bin/bash
#
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This script dumps the core mldb data into a JSON file

SCRIPTS_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
MLDB_ROOT="$(dirname "$SCRIPTS_DIR")"

if [ "$#" -gt 0 ]
then
    ARGS=("$@")
else
    ARGS=(--indent 4 -o mldb.json)
fi
"$SCRIPTS_DIR/manage.sh" dumpdata "${ARGS[@]}" mldb
