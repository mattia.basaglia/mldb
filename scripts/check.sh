#!/bin/bash
#
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This script performs sanity checks, unit tests and static analysis
# If it exists with an error code, it means there's something that needs
# to be fixed.

set -e
SCRIPTS_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
source "$SCRIPTS_DIR/activate.sh"

find "$MLDB_ROOT" -type f -name '*.pyc' -delete
python -m compileall -q "$MLDB_ROOT/apps" "$MLDB_ROOT/project"
"$SCRIPTS_DIR/manage.sh" check
"$SCRIPTS_DIR/test.sh"
"$SCRIPTS_DIR/lint.sh"
