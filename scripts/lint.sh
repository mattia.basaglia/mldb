#!/bin/bash
#
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This script runs static analysis and style checks on the Python sources

SCRIPTS_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
source "$SCRIPTS_DIR/activate.sh"

IGNORE_TEST=(
    E127 # continuation line over-indented for visual indent
    E201 # whitespace after '['
    E202 # whitespace before ']'
    E226 # missing whitespace around arithmetic operator
    E241 # multiple spaces after ':'
)

IGNORE=(
    E124 # closing bracket does not match visual indentation
    E126 # continuation line over-indented for hanging indent
    E128 # continuation line under-indented for visual indent
    E131 # continuation line unaligned for hanging indent
    E221 # multiple spaces before operator
    E227 # missing whitespace around bitwise or shift operator
    E261 # at least two spaces before inline comment
    E305 # expected 2 blank lines after class or function definition, found 0
)

EXCLUDE=(
    "env*/"
    "wikitranscript.py"
    "wsgi.py"
    "settings.py"
    "settings_debug_example.py"
    "manage.py"
    "*/migrations/*"
    "apps/mldb/data.py"
)

TEST_PATTERNS=(
    "tests.py"
    "test_*.py"
)

MAX_LINE_LENGTH=128

function csv()
{
    local IFS=","
    echo "$*"
}

flake8 \
    --max-line-length="$MAX_LINE_LENGTH" \
    --ignore="$(csv "${IGNORE[@]}")" \
    --exclude="$(csv "${EXCLUDE[@]}" "${TEST_PATTERNS[@]}")" \
    -j auto \
    "$MLDB_ROOT"

flake8 \
    --max-line-length="$MAX_LINE_LENGTH" \
    --ignore="$(csv "${IGNORE[@]}" "${IGNORE_TEST[@]}")" \
    --exclude="$(csv "${EXCLUDE[@]}")" \
    --filename="$(csv "${TEST_PATTERNS[@]}")" \
    -j auto \
    "$MLDB_ROOT"
