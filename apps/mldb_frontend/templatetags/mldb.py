"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

import six

from django import template
from django.utils.html import format_html, escape
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.templatetags.staticfiles import static

from .. import links
from ..charts import render_chart_from_args
from ...mldb import models
from ...simple_page.templatetags.simple_page import make_attrs


register = template.Library()
register.simple_tag(links.character_url)
register.simple_tag(links.episode_url)
register.simple_tag(links.season_url)


def _get_character(character):
    """
    Returns a character model from a name, id or an existing object
    """
    if isinstance(character, models.Character):
        return character
    if isinstance(character, six.string_types):
        return models.Character.objects.get(name=character)
    if isinstance(character, int):
        return models.Character.objects.get(id=character)
    return None


@register.simple_tag
def character_link(character):
    character = _get_character(character)
    return format_html(
        "<a href='{}'>{}</a>",
        links.character_url(character),
        character.name
    )


@register.simple_tag
def episode_link(episode):
    return format_html(
        "<a href='{}'>{}</a>",
        links.episode_url(episode),
        episode.title
    )


@register.simple_tag(takes_context=True)
def chart_figure(context, caption, chart_type, **kwargs):
    data = context["chart_data"]
    height = 300

    if chart_type == "pie_chart":
        padding = 2
        width = height
    else:
        padding = 6
        width = max(600, padding * 2 + 23 * len(data.records))

    chart_args = {
        "width": width,
        "height": height,
        "padding": padding,
    }
    chart_args.update(**kwargs)

    return mark_safe("""
        <figure>
            <figcaption>{caption}</figcaption>
            <div class="figure_container">{svg}</div>
            <div class="figure_links">
                <a href="{edit_url}?chart_type={chart_type}&amp;{data_query}">Edit chart</a>
                <a href="{chart_url}?{data_query}">View image</a>
            </div>
        </figure>
    """.format(
        caption=caption,
        svg=render_chart_from_args(chart_type, data, chart_args, context["request"], False),
        edit_url=reverse("chart_editor"),
        chart_url=reverse("chart", kwargs={"chart_type": chart_type}),
        chart_type=chart_type,
        data_query=escape(context["data_query"])
    ))


@register.simple_tag
def character_image_url(character, name_pattern, fallback="unknown"):
    character = _get_character(character)
    path = name_pattern % character.slug
    if not finders.find(path):
        if fallback is None:
            return None
        path = name_pattern % fallback
    return static(path)


@register.simple_tag
def character_image(character, name_pattern, size, **extra_attrs):
    character = _get_character(character)
    attrs = {
        "src": character_image_url(character, name_pattern),
        "width": size,
        "height": size,
        "alt": character.name,
        "title": character.name,
    }
    attrs.update(extra_attrs)
    return mark_safe("<img %s/>" % make_attrs(attrs))


@register.simple_tag
def character_image_link(character, name_pattern, size):
    character = _get_character(character)
    return format_html(
        "<a href='{}'>{}</a>",
        links.character_url(character),
        character_image(character, name_pattern, size)
    )
