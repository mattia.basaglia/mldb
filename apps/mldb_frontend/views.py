"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals
from __future__ import absolute_import

from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.db.models import Count
from django.conf import settings
from django.core.paginator import InvalidPage, Paginator
from django.utils.html import format_html

from .charts import chart_view, get_chart_view

from ..simple_page.page import Page, LinkGroup, Link, Resource, ViewRegistry
from ..mldb import models
from . import api
from . import charts, links, forms
from .templatetags.mldb import character_image_url


svg_css_resource = Resource(Resource.Style|Resource.Template, "mldb/svg.css")


class MldbPage(Page):
    """
    Page with site-specific defaults
    """
    site_name = "MLP Transcript"

    def __init__(self):
        super(MldbPage, self).__init__()

        if not self.block_contents:
            self.block_contents = "mldb/%s.html" % self.slug()

        self.menu = LinkGroup(self.site_name, [
            Home.link(),
            Characters.link(),
            Episodes.link(),
            Search.link(),
            Link(reverse_lazy("admin:index"), "Admin", lambda r: r.user.is_staff),
        ])

        self.footer = [
            LinkGroup(self.site_name, [
                Home.link(),
                Characters.link(),
                Episodes.link(),
                Compare.link(),
                About.link(),
            ]),
            LinkGroup("API", [
                ApiDocs.link("Documentation"),
                ChartEditor.link(),
            ]),
            LinkGroup("Sources", [
                Link("https://gitlab.com/mattia.basaglia/mldb", self.site_name),
                Link("https://gitlab.com/mattia.basaglia/Pony-Lines", "Pony-Lines"),
                Link(settings.WIKI_BASE, "MLP Wiki"),
            ]),
        ]


views = ViewRegistry()
chart_views = ViewRegistry()
chart_views.add_url(r'^chart/(?P<chart_type>[a-z_]+)\.svg$', chart_view, name="chart")
chart_views.add_url(r'^chart.svg$', get_chart_view, name="chart_generic")
views.add_pattern(chart_views, 0.5)
api_views = ViewRegistry()


def season_episodes(season):
    """
    Returns a queryset with all episodes from the given season
    """
    return (
        models.Episode.objects
        .filter(id__gt=season * 100, id__lt=(season + 1) * 100)
        .order_by("id")
    )


def seasons_context():
    """
    Returns a list that can be used in a render context to display all episodes
    divided by season
    """
    latest_episode = models.Episode.objects.latest("id")
    latest_season = latest_episode.season if latest_episode else 0
    return {
        "seasons": [
            {
                "number": season,
                "episodes": season_episodes(season),
            }
            for season in range(1, latest_season + 1)
        ]
    }


def cutoff(characters, threshold=10):
    return characters[:threshold], characters[threshold:]


def comparison_context(comparison, expand_characters=False):
    characters = list(comparison.characters)
    if expand_characters and comparison.other:
        characters += comparison.other
    return {
        "characters": characters,
        "episodes": comparison.episodes,
        "chart_data": comparison.data(),
        "data_query": comparison.query_string()
    }


@views.register(r'^$')
class Home(MldbPage):
    """
    Homepage view
    """
    def context(self, request):
        ctx = {
            "n_characters": models.Character.objects.count(),
            "n_lines": models.Line.objects.count(),
            "n_episodes": models.Episode.objects.count(),
            "best": models.annotate_characters(models.Character.objects).first(),
            "wiki_url": settings.WIKI_BASE,
        }
        ctx.update(seasons_context())
        return ctx


@views.register(r'^about/$')
class About(MldbPage):
    """
    About view
    """
    def context(self, request):
        ctx = {
            "wiki_url": settings.WIKI_BASE,
        }
        return ctx


@views.register(r'^episodes/$')
class Episodes(MldbPage):
    """
    Episode list
    """
    block_contents = "mldb/episode_list.html"

    def __init__(self):
        super(Episodes, self).__init__()

        self.breadcrumbs = LinkGroup([
            Episodes.link(),
            Link(reverse_lazy("admin:mldb_episode_changelist"), "Edit", "mldb.change_episode"),
        ])

    def context(self, request):
        return seasons_context()


@views.register(r'^episodes/(?P<season>[0-9]+)/$')
class Season(MldbPage):
    """
    List of episodes in the given season
    """
    def __init__(self, season):
        super(Season, self).__init__()
        self.season = int(season)
        self.breadcrumbs = LinkGroup([
            Episodes.link(),
            Link(links.season_url(self.season), "Season %s" % self.season)
        ])
        self.title = "Season %s" % self.season
        self.resources.append(svg_css_resource)

    def context(self, request):
        characters = models.annotate_characters(
            models.Character.objects
            .filter(line__episode__gt=self.season * 100,
                    line__episode__lt=(self.season + 1) * 100)
        ).distinct()

        episodes = season_episodes(self.season)
        characters = list(characters)
        detailed, other = cutoff(characters)
        comparison = charts.ComparisonData(episodes, detailed, other)

        ctx = comparison_context(comparison, True)
        ctx["season"] = "%02i" % self.season
        return ctx


@views.register(r'^episodes/(?P<season>[0-9]+)/(?P<number>[0-9]+)/$')
class Episode(MldbPage):
    """
    Episode details
    """

    def __init__(self, season, number):
        super(Episode, self).__init__()

        self.season = int(season)
        self.number = int(number)
        self.episode = get_object_or_404(
            models.Episode,
            id=models.Episode.make_id(self.season, self.number)
        )
        self.title = self.episode.title
        self.breadcrumbs = LinkGroup([
            Episodes.link(),
            Link(links.season_url(self.season), "Season %s" % self.season),
            Link(links.episode_url(self.episode), self.episode.title),
            Link(reverse("admin:mldb_episode_change", args=[self.episode.id]),
                 "Edit", "mldb.change_episode"),
        ])
        self.resources += [
            svg_css_resource,
            Resource(Resource.Style|Resource.Template, "mldb/transcript.css"),
        ]

    def context(self, request):
        characters = list(models.annotate_characters(
            models.Character.objects.filter(line__episode=self.episode)
        ).distinct())

        detailed, other = cutoff(characters)
        comparison = charts.ComparisonData([self.episode], detailed, other)

        ctx = {
            "episode": self.episode,
            "lines": list(models.Line.objects
                .filter(episode=self.episode)
                .prefetch_related("characters")
                .order_by("order")),
            "wiki_url": settings.WIKI_BASE,
        }
        ctx.update(comparison_context(comparison, True))
        return ctx


@views.register(r'^characters/$')
class Characters(MldbPage):
    """
    Full list of characters
    """
    block_contents = "mldb/character_list.html"

    def __init__(self):
        super(Characters, self).__init__()

        self.breadcrumbs = LinkGroup([
            Characters.link(),
            Compare.link(),
            Link(reverse("admin:mldb_character_changelist"), "Edit", "mldb.change_character")
        ])
        self.resources.append(svg_css_resource)

    def context(self, request):
        characters = list(models.annotate_characters(models.Character.objects))
        detailed, other = cutoff(characters)
        episodes = models.Episode.objects.order_by("id")
        comparison = charts.ComparisonData(episodes, detailed, other)
        return comparison_context(comparison, True)


@views.weighted_register(1, r'^(?P<name>.*?)/$')
class Character(MldbPage):
    """
    Character details
    """

    def __init__(self, name):
        super(Character, self).__init__()

        self.character = get_object_or_404(models.Character, name=name)
        self.title = self.character.name
        self.breadcrumbs = LinkGroup([
            Characters.link(),
            Link(links.character_url(self.character), name),
            Link(reverse("admin:mldb_character_change", args=[self.character.id]),
                 "Edit", "mldb.change_character"),
        ])
        self.resources.append(svg_css_resource)
        self.resources.append(Resource(Resource.Static|Resource.Style, "mldb/annoyed.css"))

        icon = character_image_url(self.character, "mldb/img/cutie_marks/%s.svg", None)
        if icon: # pragma: no cover
            self.resources.append(Resource(Resource.Icon|Resource.Url, icon))
            self.title_template = format_html(
                "<img alt='{}' src='{}' />",
                self.character.name,
                icon
            ) + "{title}"

    def context(self, request):
        episodes = models.Episode.objects.order_by("id")
        episodes = episodes \
            .filter(line__characters__in=[self.character]) \
            .annotate(n_lines=Count('id')) \
            .distinct()

        comparison = charts.ComparisonData(episodes, [self.character], None)

        ctx = {
            "character": self.character,
            "line_count": sum(episodes.values_list("n_lines", flat=True)),
        }
        ctx.update(comparison_context(comparison))
        return ctx


@views.register(r'^search/$')
class Search(MldbPage):
    max_results_per_page = 200

    def __init__(self):
        super(Search, self).__init__()
        self.resources += [
            Resource(Resource.Style|Resource.Static, "mldb/search.css"),
            Resource(Resource.Script|Resource.Static, "mldb/search.js"),
        ]

    def context(self, request):
        results = None

        if request.GET:
            form = forms.SearchForm(request.GET)
            if form.is_valid():
                results_per_page = min(
                    form.cleaned_data["results_per_page"],
                    self.max_results_per_page
                )
                paginator = Paginator(form.search(), results_per_page)
                try:
                    results = paginator.page(request.GET.get("page"))
                except InvalidPage:
                    results = paginator.page(1)
        else:
            form = forms.SearchForm()

        if "page" in request.GET:
            query = request.GET.copy()
            del query["page"]
        else:
            query = request.GET

        return {
            "form": form,
            "results": results,
            "query_string": query.urlencode(),
            "query": query.get("q", ""),
        }


@views.register(r'^characters/compare/$')
class Compare(MldbPage):
    title = "Compare Characters"

    def __init__(self):
        super(Compare, self).__init__()
        self.breadcrumbs = LinkGroup([
            Characters.link(),
            Compare.link(),
        ])
        self.resources += [
            svg_css_resource,
            Resource(Resource.Style|Resource.Static, "mldb/search.css"),
            Resource(Resource.Script|Resource.Static, "mldb/search.js"),
        ]

    def context(self, request):
        ctx = {
            "show_results": False
        }
        if request.GET:
            form = forms.CompareForm(request.GET)
            if form.is_valid():
                characters = list(form.cleaned_data["characters"])
                episodes = form.cleaned_data["episode_range"]
                other_characters = None
                if form.cleaned_data["include_other"]:
                    other_characters = models.annotate_characters(
                        models.Character.objects.exclude(
                            id__in=[ch.id for ch in characters]
                        )
                    )

                comparison = charts.ComparisonData(
                    episodes,
                    characters,
                    other_characters,
                    form.cleaned_data["metric"]
                )
                ctx.update(comparison_context(comparison))

                ctx["show_results"] = True
        else:
            form = forms.CompareForm()

        ctx["form"] = form
        return ctx

    @classmethod
    def link(cls):
        return Link(reverse_lazy(cls.slug()), "Compare")


@chart_views.register(r'^chart/editor/$')
class ChartEditor(MldbPage):
    """
    View showing the chart editor
    """

    def __init__(self):
        super(ChartEditor, self).__init__()
        self.resources += [
            Resource(Resource.Style|Resource.Static, "mldb/search.css"),
            Resource(Resource.Script|Resource.Static, "mldb/search.js"),
        ]
        self.breadcrumbs = LinkGroup([
            ApiDocs.link(),
            ChartEditor.link(),
        ])

    def context(self, request):
        query = ""

        if request.GET:
            form_global = forms.ChartFormGlobalStyle(request.GET)
            form_data = forms.ChartFormData(request.GET)
            form_extra = forms.ChartFormExtraStyle(request.GET)
            if form_global.is_valid() and form_data.is_valid() and form_extra.is_valid():
                query = request.GET.urlencode()
        else:
            form_global = forms.ChartFormGlobalStyle()
            form_data = forms.ChartFormData()
            form_extra = forms.ChartFormExtraStyle()

        return {
            "form_global": form_global,
            "form_data": form_data,
            "form_extra": form_extra,
            "query": query,
        }


@api_views.register(r'^$')
class ApiDocs(MldbPage):
    "API Documentation"
    title = "API Documentation"

    def __init__(self):
        super(ApiDocs, self).__init__()
        self.breadcrumbs = LinkGroup([
            ApiDocs.link(),
            ChartEditor.link(),
        ])
        self.resources += [
            Resource(Resource.Style|Resource.Static, "simple_page/api.css"),
        ]

    def context(self, request):
        return {
            "api": api.api,
        }
