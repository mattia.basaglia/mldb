"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

from django.urls import reverse


def character_url(character):
    return reverse("character", kwargs={"name": character.name})


def episode_url(episode):
    return reverse("episode", kwargs={
        "season": "%02d" % episode.season,
        "number": "%02d" % episode.number,
    })


def season_url(season):
    return reverse("season", kwargs={
        "season": "%02d" % int(season),
    })
