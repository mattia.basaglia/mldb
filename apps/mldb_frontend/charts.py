"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

import hashlib

from django.template.loader import render_to_string
from django.http import HttpResponseBadRequest, QueryDict, HttpResponse
from django.core.cache import cache
from django.db.models import Sum, Case, When, IntegerField, Value
from django.conf import settings

from ..chart import charts
from ..chart.templatetags.charts import chart_types
from . import links
from ..mldb import models

max_characters = 16


class ComparisonData(object):
    """
    Class to store character/episode data
    """
    EpisodeLines = 0
    CumultativeLines = 1
    EpisodeCount = 2
    MainCharacter = 3

    def __init__(self, episodes, characters, other, metric=EpisodeLines):
        self.episodes = episodes
        if type(episodes) is list:
            self.episodes = models.Episode.objects \
                .filter(id__in=[ep.id for ep in episodes]) \
                .order_by("id")
        self.characters = characters
        self.other = other
        self.metric = metric

        hasher = hashlib.sha512()
        hasher.update(str([episode.id for episode in self.episodes]))
        hasher.update(str([character.id for character in self.characters]))
        if self.other is not None:
            hasher.update(str([character.id for character in self.other]))
        hasher.update(str(self.metric))
        hasher.update(str([
            episode.last_loaded.isoformat()
            for episode in self.episodes
        ]))
        self._hexdigest = hasher.hexdigest()

    def key(self):
        """
        Cache key for the comparison
        get refreshed
        """
        return "comparison_" + self._hexdigest

    def data(self):
        """
        Returns the (possibly cached) chart data
        """
        key = self.key()
        if key not in cache:
            value = self.evaluate()
            cache.set(key, value)
            return value
        return cache.get(key)

    def evaluate(self):
        """
        Computes the value anew (and returns it)
        """
        chart_data = self._main_data(self.characters, self.episodes)
        if self.other is not None and self.other:
            other_characters_lines = self._metric_data(self.other, self.episodes)
            chart_data.items.append(charts.MetaData("Other", "other"))
            for record, value in zip(chart_data.values, other_characters_lines):
                record.append(value)
        return chart_data

    @staticmethod
    def _count_lines(characters, episodes):
        # This query is similar to
        # .filter(line__characters__in=[character])
        # .annotate(n_lines=Count("line__id"))
        # but it keeps all of the episodes, even without matchin lines
        return list(
            episodes.annotate(n_lines=Sum(Case(
                When(line__characters__in=list(characters), then=Value(1)),
                default=Value(0),
                output_field=IntegerField()
            ))).values_list("n_lines", flat=True)
        )

    def _metric_data(self, characters, episodes):
        """
        Returns data for the current metric, as a list of values where
        returned[i] is the value corresponding to episodes[i].
        """
        def cumsum(iter):
            total = 0
            for item in iter:
                total += item
                yield total

        count_lines = ComparisonData._count_lines(characters, episodes)

        if self.metric == self.EpisodeCount:
            return list(cumsum(1 if x else 0 for x in count_lines))
        elif self.metric == self.CumultativeLines:
            return list(cumsum(count_lines))
        elif self.metric == self.MainCharacter:
            return cumsum(
                1 if set(characters) & set(episode.main_characters()) else 0
                for episode in episodes
            )
        else:
            return count_lines

    def _main_data(self, characters, episodes):
        """
        Returns a data matrix mapping episodes * characters to metric
        """
        return charts.DataMatrix(
            [
                episode_metadata(episode)
                for episode in episodes
            ],
            [
                character_metadata(character)
                for character in characters
            ],
            map(list, zip(*[
                self._metric_data([character], episodes)
                for character in characters
            ]))
        )

    def query_string(self):
        qd = QueryDict(mutable=True)
        qd.setlist("characters", [character.id for character in self.characters])
        qd.setlist("episodes", [episode.id for episode in self.episodes])
        if self.other is not None:
            qd.setlist("other", [character.id for character in self.other])
        qd["metric"] = self.metric
        return qd.urlencode()


def character_metadata(character):
    return charts.MetaData(
        character.name,
        character.slug,
        links.character_url(character),
        character
    )


def episode_metadata(episode):
    return charts.MetaData(
        episode.title,
        episode.slug,
        links.episode_url(episode),
        episode
    )


def render_chart_from_args(chart_type, data, argsdict, request=None, include_css=False):
    width = float(argsdict.get("width", 600))
    height = float(argsdict.get("height", 300))
    padding = float(argsdict.get("padding", 6))

    # This is to ensure we have a plain dict (not a querydict)
    argsdict = {k: v for k, v in argsdict.iteritems()}
    argsdict.update(
        width=width,
        height=height,
        padding=padding,
        class_prefix="character_",
        request=request,
    )

    chart_type_class = chart_types[chart_type]
    ctx = {
        "chart_type": chart_type,
        "chart_data": data,
        "width": width,
        "height": height,
        "chart": chart_type_class.template_tag()(data=data, **argsdict),
        "include_css": include_css,
    }

    return render_to_string("mldb/chart.svg", ctx, request)


def get_chart_view(request):
    """
    Like chart_view() but the chart_type is retrieved from request.GET
    """
    return chart_view(request, request.GET["chart_type"])


def chart_view(request, chart_type):
    """
    View that renders a SVG chart
    """
    try:
        if chart_type not in chart_types:
            return HttpResponseBadRequest(
                "Invalid chart type. Supported values are: %s" %
                ", ".join(chart_types.keys()),
                content_type="text/plain"
            )

        char_ids = request.GET.getlist("characters")
        if len(char_ids) > max_characters:
            return HttpResponseBadRequest(
                "Too many characters selected",
                content_type="text/plain"
            )
        characters = list(models.annotate_characters(
            models.Character.objects.filter(id__in=char_ids)
        ))

        if not characters:
            return HttpResponseBadRequest(
                "No characters selected",
                content_type="text/plain"
            )

        if "episodes" not in request.GET:
            episodes = models.Episode.objects.all()
        else:
            episodes = models.Episode.objects.filter(id__in=request.GET.getlist("episodes"))
        if not episodes:
            return HttpResponseBadRequest(
                "No episodes selected",
                content_type="text/plain"
            )

        other_characters = None
        if "other" in request.GET:
            other_characters = list(models.annotate_characters(
                models.Character.objects.exclude(id__in=char_ids)
            ))

        metric = request.GET.get("metric", "0")
        metric = int(metric) if metric.isdigit() else 0

        comparison = ComparisonData(episodes, characters, other_characters, metric)
        data = comparison.data()

        include_css = "nocss" not in request.GET

        return HttpResponse(
            render_chart_from_args(chart_type, data, request.GET,
                                   request, include_css),
            content_type="image/svg+xml"
        )

    except:
        if settings.DEBUG:
            raise
        return HttpResponseBadRequest(
            "Invalid chart parameters",
            content_type="text/plain"
        )
