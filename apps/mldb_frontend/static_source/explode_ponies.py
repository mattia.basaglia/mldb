#!/usr/bin/env python
"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import print_function

import os
import string
import argparse
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
from io import BytesIO
from xml.etree import ElementTree


def read_config(files, sections):
    """
    Reads all the config files and splits the given sections into different groups
    Notice that the section "default" is special and always read but doesn't
    result in an extra group.
    """
    expansions = {
        "script_dir": os.path.dirname(__file__)
    }
    config_parser = configparser.SafeConfigParser(expansions)
    config_parser.defaults()
    config_parser.read(files)

    # Default to all sections
    if not sections:
        sections = config_parser.sections()

    def items_to_dict(section):
        """ Splits CSV fields """
        return {
            name:
            value if name not in ["characters", "faces"] else value.split(",")
            for name, value in config_parser.items(section)
            if name not in expansions
        }

    def isxdigit(value):
        return value and all(c in string.hexdigits for c in value)

    def parse_color(value):
        """ Converts color values into hexadecimal notation """
        if "," in value or " " in value:
            return "#%02x%02x%02x" % tuple(map(int, value.replace(",", " ").split()))
        if isxdigit(value) and (len(value) == 3 or len(value) == 6):
            return "#" + value
        if (len(value) == 7 or len(value) == 4) and value[0] == '#' and isxdigit(value[1:]):
            return value

    # Extracts default values
    defaults = default_options.copy()
    defaults.update(items_to_dict("default"))

    palettes = {}
    options = []
    palette_prefix = "characters/"
    for section in sections:
        if section == "default":
            continue
        if section.startswith(palette_prefix):
            # Defines a character palette
            character_name = section[len(palette_prefix):]
            if character_name not in palettes:
                palettes[character_name] = {}
            for name, value in config_parser.items(section):
                if name not in expansions:
                    palettes[character_name][name] = parse_color(value)
        else:
            # Appends an option set, defaulting to the value in [default]
            options.append(defaults.copy())
            options[-1].update(items_to_dict(section))

    return options, palettes


def parse_arguments():
    """
    Parses command line arguments
    """
    # Parses for config-related options
    config_parser = argparse.ArgumentParser(add_help=False)
    config = config_parser.add_argument_group("Config options")
    config.add_argument(
        "--config-sections", "-s",
        help="Configuration sections to load",
        nargs="*",
        default=[]
    )
    config.add_argument(
        "--config-file",
        help="Add configuration file to load default values",
        type=list,
        action="append",
        default=[],
    )
    config.add_argument(
        "--no-config",
        help="Skip loading configuration files",
        action="store_true"
    )

    # Read config files
    config_ns, other = config_parser.parse_known_args()
    if not config_ns.no_config:
        thisdir = os.path.dirname(os.path.realpath(__file__))
        files = [os.path.join("config.ini")]
        if os.path.realpath(os.curdir) != thisdir:
            files.append("config.ini")
        files += config_ns.config_file
        options, palettes = read_config(files, config_ns.config_sections)

    # Actual argument parser
    parser = argparse.ArgumentParser(
        parents=[config_parser],
        add_help=False,
        description="Extracts multiple portrait pictures from source svgs.",
    )
    info = parser.add_argument_group("Informational")
    info.add_argument(
        "--help", "-h",
        help="Show this help message and exit",
        action="help",
    )

    # Options used to override config values
    override = parser.add_argument_group("Config Override")
    override.add_argument(
        "--prefix", "-p",
        help="Prefix for output filenames",
    )
    override.add_argument(
        "--characters", "-c",
        help="Characters to extract",
        nargs="*",
    )
    override.add_argument(
        "--faces", "-f",
        help="Faces to extract",
        nargs="*",
    )
    override.add_argument(
        "--input", "-i",
        help="Input file",
    )
    override.add_argument(
        "--format",
        help="Output format",
        choices=formats.keys(),
    )

    # Parse arguments
    namespace = parser.parse_args(other)

    # Update options sets with the overridable arguments
    overridable = set(default_options.keys())
    values = {
        key: value
        for key, value in vars(namespace).items()
        if value is not None and key in overridable
    }

    if not config_ns.no_config:
        for option_set in options:
            option_set.update(values)

        for character, attrs in palettes.items():
            palettes[character] = Palette(**attrs)

        return options, palettes
    else:
        options = default_options.copy()
        options.update(values)
        return [options], {}


class Palette(object):
    """
    Character palette
    """
    def __init__(self, coat, line, eye_1, eye_2):
        self.coat = coat
        self.line = line
        self.eye_1 = eye_1
        self.eye_2 = eye_2

    def css(self):
        """
        Returns css rules used to replace colors correctly
        """
        return """
            .ponyfill{{ fill: {coat} !important; }}
            .ponyline{{ stroke: {line} !important; }}
            .ponylinefill{{ fill: {line} !important; }}
            #eyes_gradient > stop:first-child {{ stop-color: {eye_1}; }}
            #eyes_gradient > stop:last-child {{ stop-color: {eye_2}; }}
        """.format(**vars(self))

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


def toggle_layers(list, current):
    """
    Returns css code to hide all elements in \p list and chow \p current
    """
    return "\n".join(
        ".%s { display: none; }" % item
        for item in list
        if item != current
    ) + "\n.%s { display: inline; }\n" % current


def write_svg(tree, filename, input):
    """
    Saves the svg tree as svg
    """
    tree.write(filename)


def write_png(tree, filename, input):
    """
    Saves the svg tree as a png
    NOTE: needs cairosvg>=2.0, which requires Python>=3.4 to give proper results
    """
    import cairosvg
    io = BytesIO()
    tree.write(io)
    cairosvg.svg2png(
        bytestring=io.getvalue(),
        url=input,
        write_to=filename
    )


def explode(palettes, faces, characters, input, prefix, format):
    """
    Parses the input file end extracts all the required images
    \param faces        List of face styles
    \param characters   List of character slugs
    \param input        Input svg file
    \param prefix       Prefix added to output file names
    \param format       Output format names (see formats below)
    """
    xmlns = {
        "dc"         : "http://purl.org/dc/elements/1.1/",
        "cc"         : "http://creativecommons.org/ns#",
        "rdf"        : "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "svg"        : "http://www.w3.org/2000/svg",
        "xlink"      : "http://www.w3.org/1999/xlink",
        "sodipodi"   : "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd",
        "inkscape"   : "http://www.inkscape.org/namespaces/inkscape",
    }

    tree = ElementTree.parse(input)

    for layer in tree.findall(".//svg:g[@inkscape:label]", xmlns):
        if "style" in layer.attrib:
            del layer.attrib["style"]

    for stop in tree.findall(".//svg:linearGradient[@id='eyes_gradient']/svg:stop", xmlns):
        del stop.attrib["style"]

    style = tree.find(".//svg:style", xmlns)

    for character in characters:
        palette = palettes[character]
        character_css = palette.css() + toggle_layers(characters, character)

        for face_style in faces:
            style.text = character_css + toggle_layers(faces, face_style)
            output_filename = os.path.join(prefix, face_style, character) + "." + format
            print("Creating %s" % output_filename)
            formats[format](tree, output_filename, input)


def main():
    """
    Main entry point, reads all settings and command line arguments and
    processes the result
    """
    option_list, palettes = parse_arguments()
    for options in option_list:
        if not options["input"]: # Or not found
            raise Exception("Missing input file")
        if not options["faces"] or not options["characters"]:
            raise Exception("Nothing to do for %s" % options["input"])
        explode(palettes=palettes, **options)


# The keys in this dict are guaranteed to be present in the dicts returned
# by parse_arguments() and read_config()
default_options = {
    "faces": [],
    "characters": [],
    "input": "",
    "prefix": "",
    "format": "svg",
}

# Maps format names to callables used to save the image
formats = {
    "svg": write_svg,
    "png": write_png,
}

if __name__ == "__main__":
    main()
