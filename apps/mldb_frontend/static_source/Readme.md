Generating images
=================

The script `explode_ponies.py` reads svg files and configuration and from those
generate multiple output images.


Running the script
------------------
`./explode_ponies.py` is executed with the environment python interpreter, it
supports both python 2.7 and python 3.

The script starts by determining which configuration files to process,
once all configuration is loaded the svg files are parsed and the new images
are generated.

Some values from the configuration files can be overridden from the command
line, see `./explode_ponies.py --help` for a full set of options.

The script can output svg files with the appropriate style attributes applied
or convert them to png (for this you'll need a recent version of cairosvg and
python 3).


Configuration files
===================

The script by default loads `config.ini` from its directory and from
the current directory, executing all sections.
This behaviour can be changed from the command line.


Value expansions
----------------

You can add `%(script_dir)s` to the values, which gets expanded to the
directory containing `explode_ponies.py`.


Default values
--------------

The section **[default]** is special and all of its definitions are applied to
the image descriptions.


Character palettes
------------------

To specify the colors to be used with one of the characters, define a section
called **[characters/slug]** where *slug* is the identifier for the character.

Each of these sections must have the following items:

* coat: The main color for the character
* line: Outline color
* eye_1: First eye gradient color
* eye_2: Second eye gradient color

The values of those items are the color codes, which can be expressed as:
* 3 or 6 hexadecimal digit strings, with an optional # as a first character
* rbg triplets separated by commas or spaces


Source file definitions
-----------------------

All other ini sections are interpreted as descriptions of source files.
They need the following item definitions (which can fall back to
values in **[default]** or be overridden from the command line):
* prefix: Path containing the output files
* faces: Comma-separated list of face styles
* format: **svg** or **png**, specifies the output format
* characters: Comma-separated list of characters, which must have a palette
* input: Path to the input svg file


SVG files
=========

The input file is expected to be an inkscape svg file.
The file should contain at least one svg:style element.


Visibility
----------

Layers (and other groups or objects) can be attributed css classes corresponding
to character slugs or face style names, this will cause them to be displayed or
hidden on the final image.

Note that all layers will be shown by default if there is no class that hides
them, but this is not true for other types of groups, you can change this
with custom css rules.


Colors
------

Paths and other visible elements, can have css classes to change their color
in a way to match the character palette:
* ponyfill: Gives the element a fill color matching the character coat
* ponyline: Gives the element a stroke color matching the character outline
* ponylinefill: Gives the element a fill color matching the character outline


Eye gradient
------------

The gradient used for the eyes should have the id `eyes_gradient` and contain
exactly two stops.
