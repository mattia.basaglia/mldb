"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from mock import MagicMock, patch

from django.test import TestCase, SimpleTestCase
from django.test.client import RequestFactory
from django.db.models.query import QuerySet
from django.http.request import QueryDict

from ...mldb.tests import setup_lines
from ...mldb import models
from .. import api
from .. import views
from ..charts import ComparisonData


def listify_context(context):
    if isinstance(context, QuerySet):
        return list(context)
    elif type(context) is dict:
        for name, value in context.iteritems():
            context[name] = listify_context(value)
    elif type(context) is list:
        return [listify_context(item) for item in context]

    return context


class TestFunctions(TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)
        season_2 = models.Episode(id=201, title="Season2", slug="season2")
        season_2.save()
        cls.episodes.append(season_2)

    def test_season_episodes(self):
        self.assertListEqual(
            list(views.season_episodes(1)),
            self.episodes[:4]
        )

    def test_seasons_context(self):
        self.assertDictEqual(
            listify_context(views.seasons_context()),
            {
                "seasons": [
                    {
                        "number": 1,
                        "episodes": self.episodes[:4],
                    },
                    {
                        "number": 2,
                        "episodes": self.episodes[4:],
                    },
                ]
            }
        )

    def test_cutoff(self):
        self.assertEqual(
            views.cutoff(self.characters, 2),
            (self.characters[:2], self.characters[2:])
        )

    def test_comparison_context(self):
        cmp = ComparisonData(self.episodes, self.characters[:2], self.characters[2:])
        context = listify_context(views.comparison_context(cmp, False))
        self.assertDictContainsSubset(
            {
                "characters": self.characters[:2],
                "episodes": self.episodes,
            },
            context
        )
        self.assertIn("chart_data", context)
        self.assertIn("data_query", context)

    def test_comparison_context_expanded(self):
        cmp = ComparisonData(self.episodes, self.characters[:2], self.characters[2:])
        context = listify_context(views.comparison_context(cmp, True))
        self.assertDictContainsSubset(
            {
                "characters": self.characters,
                "episodes": self.episodes,
            },
            context
        )
        self.assertIn("chart_data", context)
        self.assertIn("data_query", context)


class TestViews(TestCase):
    factory = RequestFactory()
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def get_context(self, view):
        return listify_context(view.context(self.factory.get("")))

    def test_home_context(self):
        self.assertDictEqual(
            self.get_context(views.Home()),
            {
                "n_characters": len(self.characters),
                "n_lines": models.Line.objects.count(),
                "n_episodes": len(self.episodes),
                "best": self.characters[0],
                "seasons": [
                    {
                        "number": 1,
                        "episodes": self.episodes,
                    },
                ],
                "wiki_url": views.settings.WIKI_BASE,
            }
        )

    def test_about_context(self):
        self.assertDictEqual(
            self.get_context(views.About()),
            {
                "wiki_url": views.settings.WIKI_BASE,
            }
        )

    def test_episodes_context(self):
        self.assertDictEqual(
            self.get_context(views.Episodes()),
            {
                "seasons": [
                    {
                        "number": 1,
                        "episodes": self.episodes,
                    },
                ]
            }
        )

    def test_season_context(self):
        context = self.get_context(views.Season(1))
        self.assertDictContainsSubset(
            {
                "characters": [self.characters[i] for i in [0, 2, 1]],
                "episodes": self.episodes,
                "season": "01",
            },
            context
        )
        self.assertIn("chart_data", context)
        self.assertIn("data_query", context)

    def test_episode_context(self):
        context = self.get_context(views.Episode(1, 1))
        self.assertDictContainsSubset(
            {
                "episodes": [self.episodes[0]],
                "episode": self.episodes[0],
            },
            context
        )
        self.assertIn("chart_data", context)
        self.assertIn("data_query", context)
        self.assertSetEqual(set(context["characters"]), set(self.characters))

    def test_characters_context(self):
        context = self.get_context(views.Characters())
        self.assertDictContainsSubset(
            {
                "episodes": self.episodes,
            },
            context
        )
        self.assertIn("chart_data", context)
        self.assertIn("data_query", context)
        self.assertSetEqual(set(context["characters"]), set(self.characters))

    def test_character_context(self):
        context = self.get_context(views.Character("Ch1"))
        self.assertDictContainsSubset(
            {
                "characters": [self.characters[1]],
                "episodes": [self.episodes[0], self.episodes[3]],
                "character": self.characters[1],
                "line_count": 7+8,
            },
            context
        )
        self.assertIn("chart_data", context)
        self.assertIn("data_query", context)

    def test_apidocs_context(self):
        context = self.get_context(views.ApiDocs())
        self.assertDictEqual(
            {
                "api": api.api,
            },
            context
        )


class TestSearchView(SimpleTestCase):
    factory = RequestFactory()
    maxDiff = None

    def get_context(self, data):
        return listify_context(views.Search().context(self.factory.get("", data)))

    def test_noget(self):
        context = self.get_context({})
        self.assertIsInstance(context["form"], views.forms.SearchForm)
        self.assertDictEqual(
            context,
            {
                "form": context["form"],
                "results": None,
                "query_string": "",
                "query": "",
            }
        )

    def test_invalid(self):
        with patch("apps.mldb_frontend.forms.SearchForm") as SearchForm:
            form = MagicMock()
            form.is_valid.return_value = False
            SearchForm.return_value = form
            context = self.get_context({"q": "foo"})
            SearchForm.assert_called_with(QueryDict("q=foo"))
            self.assertDictEqual(
                context,
                {
                    "form": form,
                    "results": None,
                    "query_string": "q=foo",
                    "query": "foo",
                }
            )

    def test_valid(self):
        with patch("apps.mldb_frontend.forms.SearchForm") as SearchForm:
            form = MagicMock()
            form.cleaned_data = {"results_per_page": 3}
            form.search.return_value = range(6)
            SearchForm.return_value = form
            context = self.get_context({"q": "foo"})
            SearchForm.assert_called_with(QueryDict("q=foo"))
            self.assertDictContainsSubset(
                {
                    "form": form,
                    "query_string": "q=foo",
                    "query": "foo",
                },
                context
            )
            self.assertEqual(context["results"].start_index(), 1)
            self.assertEqual(context["results"].end_index(), 3)

    def test_valid_page(self):
        with patch("apps.mldb_frontend.forms.SearchForm") as SearchForm:
            form = MagicMock()
            form.cleaned_data = {"results_per_page": 3}
            form.search.return_value = range(6)
            SearchForm.return_value = form
            context = self.get_context({"q": "foo", "page": 2})
            SearchForm.assert_called_with(QueryDict("q=foo&page=2"))
            self.assertDictContainsSubset(
                {
                    "form": form,
                    "query_string": "q=foo",
                    "query": "foo",
                },
                context
            )
            self.assertEqual(context["results"].start_index(), 4)
            self.assertEqual(context["results"].end_index(), 6)

    def test_valid_wrong_page(self):
        with patch("apps.mldb_frontend.forms.SearchForm") as SearchForm:
            form = MagicMock()
            form.cleaned_data = {"results_per_page": 3}
            form.search.return_value = range(6)
            SearchForm.return_value = form
            context = self.get_context({"q": "foo", "page": 20})
            SearchForm.assert_called_with(QueryDict("q=foo&page=20"))
            self.assertDictContainsSubset(
                {
                    "form": form,
                    "query_string": "q=foo",
                    "query": "foo",
                },
                context
            )
            self.assertEqual(context["results"].start_index(), 1)
            self.assertEqual(context["results"].end_index(), 3)


class TestCompareView(TestCase):
    factory = RequestFactory()

    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def get_context(self, data):
        return listify_context(views.Compare().context(self.factory.get("", data)))

    def test_noget(self):
        context = self.get_context({})
        self.assertFalse(context["show_results"])
        self.assertIsInstance(context["form"], views.forms.CompareForm)

    def test_invalid(self):
        context = self.get_context({"foo": "bar"})
        self.assertFalse(context["show_results"])
        self.assertIsInstance(context["form"], views.forms.CompareForm)

    def test_no_other(self):
        context = self.get_context({
            "characters": [1, 2],
            "episode_start": self.episodes[0].id,
            "episode_finish": self.episodes[-1].id,
            "include_other": False,
            "metric": 0,
        })
        self.assertTrue(context["show_results"])
        self.assertIsInstance(context["form"], views.forms.CompareForm)
        self.assertDictContainsSubset(
            {
                "characters": self.characters[:2],
                "episodes": self.episodes,
            },
            context
        )
        self.assertEqual(len(context["chart_data"].items), 2)
        self.assertIn("data_query", context)

    def test_include_other(self):
        context = self.get_context({
            "characters": [1, 2],
            "episode_start": self.episodes[0].id,
            "episode_finish": self.episodes[-1].id,
            "include_other": True,
            "metric": 0,
        })
        self.assertTrue(context["show_results"])
        self.assertIsInstance(context["form"], views.forms.CompareForm)
        self.assertDictContainsSubset(
            {
                "characters": self.characters[:2],
                "episodes": self.episodes,
            },
            context
        )
        self.assertEqual(len(context["chart_data"].items), 3)
        self.assertIn("data_query", context)


class TestChartEditorView(SimpleTestCase):
    factory = RequestFactory()
    maxDiff = None

    class FormsMock(object):
        def __init__(self):
            self.form_global = MagicMock()
            self.form_data = MagicMock()
            self.form_extra = MagicMock()
            self.patches = [
                self.make_patch("ChartFormGlobalStyle", self.form_global),
                self.make_patch("ChartFormData", self.form_data),
                self.make_patch("ChartFormExtraStyle", self.form_extra)
            ]

        def make_patch(self, name, object):
            return patch("apps.mldb_frontend.forms." + name, return_value=object)

        def __enter__(self, *args, **kwargs):
            self.classes = []
            for patch_manager in self.patches:
                self.classes.append(patch_manager.__enter__(*args, **kwargs))
            return self

        def __exit__(self, *args, **kwargs):
            del self.classes
            for patch_manager in self.patches:
                patch_manager.__exit__(*args, **kwargs)

    def get_context(self, data):
        return listify_context(views.ChartEditor().context(self.factory.get("", data)))

    def test_noget(self):
        with self.FormsMock() as forms:
            context = self.get_context({})
            for cls in forms.classes:
                cls.assert_called_with()
            self.assertDictEqual(
                context,
                {
                    "form_global": forms.form_global,
                    "form_data": forms.form_data,
                    "form_extra": forms.form_extra,
                    "query": "",
                }
            )

    def test_allvalid(self):
        with self.FormsMock() as forms:
            context = self.get_context({"foo": "bar"})
            for cls in forms.classes:
                cls.assert_called_with(QueryDict("foo=bar"))
            self.assertDictEqual(
                context,
                {
                    "form_global": forms.form_global,
                    "form_data": forms.form_data,
                    "form_extra": forms.form_extra,
                    "query": "foo=bar",
                }
            )

    def test_invalid_global(self):
        with self.FormsMock() as forms:
            forms.form_global.is_valid.return_value = False
            context = self.get_context({"foo": "bar"})
            for cls in forms.classes:
                cls.assert_called_with(QueryDict("foo=bar"))
            self.assertDictEqual(
                context,
                {
                    "form_global": forms.form_global,
                    "form_data": forms.form_data,
                    "form_extra": forms.form_extra,
                    "query": "",
                }
            )

    def test_invalid_data(self):
        with self.FormsMock() as forms:
            forms.form_data.is_valid.return_value = False
            context = self.get_context({"foo": "bar"})
            for cls in forms.classes:
                cls.assert_called_with(QueryDict("foo=bar"))
            self.assertDictEqual(
                context,
                {
                    "form_global": forms.form_global,
                    "form_data": forms.form_data,
                    "form_extra": forms.form_extra,
                    "query": "",
                }
            )

    def test_invalid_extra(self):
        with self.FormsMock() as forms:
            forms.form_extra.is_valid.return_value = False
            context = self.get_context({"foo": "bar"})
            for cls in forms.classes:
                cls.assert_called_with(QueryDict("foo=bar"))
            self.assertDictEqual(
                context,
                {
                    "form_global": forms.form_global,
                    "form_data": forms.form_data,
                    "form_extra": forms.form_extra,
                    "query": "",
                }
            )
