"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from mock import patch, MagicMock

from django.test import SimpleTestCase, TestCase
from django.utils.safestring import SafeText
from django.test.client import RequestFactory

from haystack.query import EmptySearchQuerySet

from ...mldb import models
from ...mldb.tests import setup_lines
from ...chart import charts
from .. import links
from ..templatetags import mldb as tt
from .. import forms
from ..charts import ComparisonData


class TestLinks(SimpleTestCase):
    def test_character_url(self):
        self.assertEqual(
            links.character_url(models.Character(name="Foo")),
            "/Foo/"
        )

    def test_season_url(self):
        self.assertEqual(
            links.season_url(2),
            "/episodes/02/"
        )

    def test_episode_url(self):
        self.assertEqual(
            links.episode_url(models.Episode(id=203)),
            "/episodes/02/03/"
        )

    def test_character_link(self):
        link = tt.character_link(models.Character(name="Foo"))
        self.assertEqual(link, "<a href='/Foo/'>Foo</a>")
        self.assertIsInstance(link, SafeText)

    def test_episode_link(self):
        link = tt.episode_link(models.Episode(id=203, title="Foo"))
        self.assertEqual(link, "<a href='/episodes/02/03/'>Foo</a>")
        self.assertIsInstance(link, SafeText)


class TestCharacterImage(SimpleTestCase):
    character = models.Character(name="Foo", slug="bar")

    def test_character_image_url(self):
        with patch.object(tt.finders, "find", return_value="foobar") as find, \
             patch.object(tt, "static", return_value="barfoo") as static:
            url = tt.character_image_url(self.character, "pattern %s")
            find.assert_called_with("pattern bar")
            static.assert_called_with("pattern bar")
            self.assertEqual(url, "barfoo")

    def test_character_image_url_notfound(self):
        with patch.object(tt.finders, "find", return_value="") as find, \
             patch.object(tt, "static", return_value="barfoo") as static:
            url = tt.character_image_url(self.character, "pattern %s")
            find.assert_called_with("pattern bar")
            static.assert_called_with("pattern unknown")
            self.assertEqual(url, "barfoo")

    def test_character_image_url_nofallback(self):
        with patch.object(tt.finders, "find", return_value="") as find, \
             patch.object(tt, "static", return_value="barfoo") as static:
            url = tt.character_image_url(self.character, "pattern %s", None)
            find.assert_called_with("pattern bar")
            static.assert_not_called()
            self.assertIsNone(url)

    def test_character_image(self):
        with patch.object(tt, "character_image_url", return_value="foobar") as cimgurl:
            img = tt.character_image(self.character, "pattern", 32)
            cimgurl.assert_called_with(self.character, "pattern")
            self.assertHTMLEqual(
                img,
                "<img src='foobar' width='32' height='32' alt='Foo' title='Foo'/>"
            )
            self.assertIsInstance(img, SafeText)

    def test_character_image_link(self):
        with patch.object(tt, "character_image", return_value="foobar") as cimg:
            img = tt.character_image_link(self.character, "pattern", 32)
            cimg.assert_called_with(self.character, "pattern", 32)
            self.assertEqual(
                img,
                "<a href='/Foo/'>foobar</a>"
            )
            self.assertIsInstance(img, SafeText)


class TestChartFigure(SimpleTestCase):
    def patch_render(self):
        return patch.object(tt, "render_chart_from_args", return_value="svgout")

    def test_chart_figure_pie(self):
        data = charts.DataMatrix(
            [charts.MetaData("ep%i" % i) for i in xrange(5)],
            [charts.MetaData("ch%i" % i) for i in xrange(3)],
            [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        )
        request = RequestFactory().get("/foo")
        context = {
            "request": request,
            "chart_data": data,
            "data_query": "foo=bar",
        }
        with self.patch_render() as render:
            rendered = tt.chart_figure(context, "caption", "pie_chart", foo="bar")
            self.assertIn("svgout", rendered)
            self.assertIn("/chart/pie_chart.svg?foo=bar", rendered)
            chart_args = {
                "width": 300,
                "height": 300,
                "padding": 2,
                "foo": "bar"
            }
            render.assert_called_with("pie_chart", data, chart_args, request, False)

    def test_chart_figure_line(self):
        data = charts.DataMatrix(
            [charts.MetaData("ep%i" % i) for i in xrange(5)],
            [charts.MetaData("ch%i" % i) for i in xrange(3)],
            [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        )
        request = RequestFactory().get("/foo")
        context = {
            "request": request,
            "chart_data": data,
            "data_query": "foo=bar",
        }
        with self.patch_render() as render:
            rendered = tt.chart_figure(context, "caption", "line_chart", foo="bar")
            self.assertIn("svgout", rendered)
            self.assertIn("/chart/line_chart.svg?foo=bar", rendered)
            chart_args = {
                "width": 600,
                "height": 300,
                "padding": 6,
                "foo": "bar"
            }
            render.assert_called_with("line_chart", data, chart_args, request, False)

    def test_chart_figure_line_long(self):
        data = charts.DataMatrix(
            [charts.MetaData("ep%i" % i) for i in xrange(300)],
            [charts.MetaData("ch%i" % i) for i in xrange(3)],
            [[1, 2, 3] * 300]
        )
        request = RequestFactory().get("/foo")
        context = {
            "request": request,
            "chart_data": data,
            "data_query": "foo=bar",
        }
        with self.patch_render() as render:
            rendered = tt.chart_figure(context, "caption", "line_chart", foo="bar")
            self.assertIn("svgout", rendered)
            self.assertIn("/chart/line_chart.svg?foo=bar", rendered)
            chart_args = {
                "width": 6 * 2 + 23 * 300,
                "height": 300,
                "padding": 6,
                "foo": "bar"
            }
            render.assert_called_with("line_chart", data, chart_args, request, False)


class TestFormFields(TestCase):
    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def test_metric_field(self):
        metric_field = forms.metric_field()
        self.assertIsInstance(metric_field, forms.forms.TypedChoiceField)
        choices = set(choince[0] for choince in metric_field.choices)
        metrics = set((
            ComparisonData.EpisodeLines,
            ComparisonData.CumultativeLines,
            ComparisonData.EpisodeCount,
            ComparisonData.MainCharacter,
        ))
        self.assertSetEqual(choices, metrics)
        self.assertEqual(metric_field.coerce, int)

    def test_episode_field(self):
        self.assertListEqual(
            list(forms.EpisodeField.default_queryset().values_list("id", flat=True)),
            range(101, 105)
        )
        self.assertEqual(
            len(forms.EpisodeField(
                models.Episode.objects.filter(id__lt=103)
            ).queryset),
            2
        )
        self.assertListEqual(
            list(forms.EpisodeField().queryset),
            list(self.episodes)
        )
        self.assertEqual(
            forms.EpisodeField().to_python(self.episodes[0]),
            self.episodes[0]
        )
        self.assertEqual(
            forms.EpisodeField().to_python(self.episodes[0].id),
            self.episodes[0]
        )

    def test_multiple_model_field_pluralize(self):
        mmf = forms.MultipleModelField(models.Episode.objects.all())
        self.assertEqual(mmf._pluralize(1), "1 episode")
        self.assertEqual(mmf._pluralize(2), "2 episodes")
        self.assertEqual(mmf._pluralize(0), "0 episodes")

    def test_multiple_model_field_clean_ok(self):
        mmf = forms.MultipleCharactersField()
        clean = mmf.clean([1, 2])
        self.assertListEqual(
            list(clean.values("id", "name", "n_lines", "episodes")),
            [{"id": 1, "name": "Ch0", "n_lines": 15+4+7, "episodes": 3},
            {"id": 2, "name": "Ch1", "n_lines": 7+8, "episodes": 2}]
        )

    def test_multiple_model_field_clean_too_few(self):
        mmf = forms.MultipleCharactersField(min=5)
        self.assertRaises(forms.forms.ValidationError, lambda: mmf.clean([1, 2]))

    def test_multiple_model_field_clean_too_many(self):
        mmf = forms.MultipleCharactersField(max=1)
        self.assertRaises(forms.forms.ValidationError, lambda: mmf.clean([1, 2]))

    def test_multiple_characters_field_ctor_query(self):
        mmf = forms.MultipleCharactersField(
            models.Character.objects.filter(id__lt=3)
        )
        self.assertEqual(len(mmf.queryset), 2)

    def test_multiple_characters_field_ctor_default(self):
        mmf = forms.MultipleCharactersField()
        self.assertEqual(len(mmf.queryset), len(self.characters))
        self.assertTrue(hasattr(mmf.queryset[0], "n_lines"))


class TestFormBase(SimpleTestCase):
    class FormTest(forms.FormBase):
        field1 = forms.forms.CharField(initial="foo", required=False)
        field2 = forms.forms.CharField(required=False)

    def test_init_nodata(self):
        form = self.FormTest()
        self.assertFalse(form.is_valid())
        self.assertDictEqual(form.data, {})

    def test_initial_default(self):
        form = self.FormTest({"field2": "bar"})
        self.assertTrue(form.is_valid())
        self.assertDictEqual(form.cleaned_data, {"field1": "foo", "field2": "bar"})

    def test_initial_data(self):
        form = self.FormTest({"field1": "bar"})
        self.assertTrue(form.is_valid())
        self.assertDictEqual(form.cleaned_data, {"field1": "bar", "field2": ""})

    def test_initial_args(self):
        form = self.FormTest({}, initial={"field2": "bar"})
        self.assertTrue(form.is_valid())
        self.assertDictEqual(form.cleaned_data, {"field1": "foo", "field2": "bar"})

    def test_as_div_div(self):
        form = self.FormTest()
        html = form.as_div()
        self.assertHTMLEqual(
            html,
            '''
            <div>
                <label for="id_field1">Field1:</label>
                <input type="text" value="foo" name="field1" id="id_field1"/>
            </div>
            <div>
                <label for="id_field2">Field2:</label>
                <input type="text" name="field2" id="id_field2"/>
            </div>
            '''
        )

    def test_as_div_span(self):
        form = self.FormTest()
        html = form.as_div("span")
        self.assertHTMLEqual(
            html,
            '''
            <span>
                <label for="id_field1">Field1:</label>
                <input type="text" value="foo" name="field1" id="id_field1"/>
            </span>
            <span>
                <label for="id_field2">Field2:</label>
                <input type="text" name="field2" id="id_field2"/>
            </span>
            '''
        )


class TestSearchForm(SimpleTestCase):
    def test_highlighter_insane_window(self):
        window = forms.SearchHighlighter("").find_window(None)
        self.assertEqual(window[0], 0)
        self.assertGreater(window[1], 1024)

    def test_ctor_nodata(self):
        sf = forms.SearchForm()
        self.assertEqual(sf.data, {})
        self.assertFalse(sf.is_valid())

    def test_ctor_default_paging(self):
        sf = forms.SearchForm({})
        self.assertEqual(sf.data["results_per_page"], sf.result_count_default)

    def test_ctor_explicit_paging(self):
        sf = forms.SearchForm({"results_per_page": 4})
        self.assertEqual(sf.data["results_per_page"], 4)

    def patch_search(self, *args, **kwargs):
        return patch.object(
            forms.haystack.forms.SearchForm, "search", *args, **kwargs
        )

    def test_search_invalid(self):
        sf = forms.SearchForm()
        with self.patch_search() as search:
            self.assertIsInstance(sf.search(), EmptySearchQuerySet)
            search.assert_not_called()

    def test_search_no_characters(self):
        sf = forms.SearchForm({"q": "foo"})
        with self.patch_search() as search:
            self.assertIsInstance(sf.search(), MagicMock)
            search.assert_called()

    def test_search_characters(self):
        sf = forms.SearchForm({"q": "foo", "characters": [1, 2, 3]})
        queryset = MagicMock()
        sf.cleaned_data = {"characters": [1, 2, 3]}
        sf.is_valid = MagicMock()
        with self.patch_search(return_value=queryset) as search:
            self.assertIsInstance(sf.search(), MagicMock)
            queryset.filter.assert_called_with(characters__in=[1, 2, 3])
            search.assert_called()


class TestCompareForm(TestCase):
    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def test_initial(self):
        cmp = forms.CompareForm()
        self.assertEqual(cmp.initial["episode_start"], self.episodes[0])
        self.assertEqual(cmp.initial["episode_finish"], self.episodes[-1])

    def test_clean_swap(self):
        cmp = forms.CompareForm({
            "characters": [1, 2],
            "episode_start": self.episodes[2],
            "episode_finish": self.episodes[1],
        })
        self.assertTrue(cmp.is_valid())
        self.assertListEqual(
            list(cmp.cleaned_data["episode_range"]),
            self.episodes[1:3]
        )

    def test_clean_no_swap(self):
        cmp = forms.CompareForm({
            "characters": [1, 2],
            "episode_start": self.episodes[1],
            "episode_finish": self.episodes[2],
        })
        self.assertTrue(cmp.is_valid())
        self.assertListEqual(
            list(cmp.cleaned_data["episode_range"]),
            self.episodes[1:3]
        )

    def test_clean_invalid(self):
        cmp = forms.CompareForm({
            "characters": [1, 2],
            "episode_start": 501,
            "episode_finish": self.episodes[2],
        })
        self.assertFalse(cmp.is_valid())
        self.assertNotIn("episode_range", cmp.cleaned_data)


class TestGetCharacter(TestCase):
    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def test_plain_object(self):
        self.assertIs(tt._get_character(self.characters[0]), self.characters[0])

    def test_name(self):
        self.assertEqual(
            tt._get_character(self.characters[0].name),
            self.characters[0]
        )

    def test_id(self):
        self.assertEqual(
            tt._get_character(self.characters[0].id),
            self.characters[0]
        )

    def test_wtf(self):
        self.assertIsNone(tt._get_character([]))
