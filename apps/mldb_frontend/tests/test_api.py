"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.test import TestCase

from .. import api
from ...mldb.tests import setup_lines


class TestApi(TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def test_version(self):
        self.assertEqual(api.api.version(), 1)

    def test_characters(self):
        self.assertListEqual(
            api.api.characters(),
            [
                {
                    "id": 1,
                    "name": "Ch0",
                    "slug": "ch_0",
                    "color": "#ffffff",
                    "outline": "#cccccc",
                },
                {
                    "id": 2,
                    "name": "Ch1",
                    "slug": "ch_1",
                    "color": "#ffffff",
                    "outline": "#cccccc",
                },
                {
                    "id": 3,
                    "name": "Ch2",
                    "slug": "ch_2",
                    "color": "#ffffff",
                    "outline": "#cccccc",
                },
            ]
        )

    def test_character_by_id(self):
        self.assertDictEqual(
            api.api.character_by_id(1),
            {
                "id": 1,
                "name": "Ch0",
                "slug": "ch_0",
                "color": "#ffffff",
                "outline": "#cccccc",
                "episodes": [
                    "01/01",
                    "01/02",
                    "01/04",
                ]
            },
        )

    def test_character(self):
        self.assertDictEqual(
            api.api.character("Ch0"),
            {
                "id": 1,
                "name": "Ch0",
                "slug": "ch_0",
                "color": "#ffffff",
                "outline": "#cccccc",
                "episodes": [
                    "01/01",
                    "01/02",
                    "01/04",
                ]
            },
        )

    def test_episodes(self):
        self.assertListEqual(
            api.api.episodes(),
            [
                {
                    "title": "Ep%s" % i,
                    "slug": "ep_%s" % i,
                    "season": "01",
                    "episode": "0%s" % i,
                }
                for i in xrange(1, 5)
            ]
        )

    def test_season(self):
        self.assertDictEqual(
            api.api.season(1),
            {

                "season": "01",
                "episodes": [
                    {
                        "title": "Ep%s" % i,
                        "slug": "ep_%s" % i,
                        "episode": "0%s" % i,
                    }
                    for i in xrange(1, 5)
                ],
            }
        )

    def test_episode(self):
        self.assertDictEqual(
            api.api.episode(1, 2),
            {
                "title": "Ep2",
                "slug": "ep_2",
                "episode": "02",
                "season": "01",
                "characters": [
                    "Ch0", "Ch2"
                ]
            }
        )

    def test_lines(self):
        self.assertDictEqual(
            api.api.lines(1, 2),
            {
                "title": "Ep2",
                "slug": "ep_2",
                "episode": "02",
                "season": "01",
                "lines":
                    [{"text": "foo", "characters": ["Ch0"]}] * 4 +
                    [{"text": "foo", "characters": ["Ch2"]}] * 10
            }
        )
