"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from xml.etree import ElementTree

from mock import patch

from django.test import SimpleTestCase, TestCase
from django.db.models.query import QuerySet
from django.core.cache.backends.locmem import LocMemCache
from django.test.client import RequestFactory
from django.http import QueryDict

from ...mldb import models
from ...mldb.tests import setup_lines
from .. import charts


class TestChartFunctions(SimpleTestCase):
    def test_character_metadata(self):
        character = models.Character(name="foo", slug="bar")
        chmd = charts.character_metadata(character)
        self.assertEqual(chmd.label, "foo")
        self.assertEqual(chmd.id, "bar")
        self.assertEqual(chmd.link, "/foo/")
        self.assertEqual(chmd.extra, character)

    def test_episode_metadata(self):
        episode = models.Episode(id=103, title="foo", slug="bar")
        chmd = charts.episode_metadata(episode)
        self.assertEqual(chmd.label, "foo")
        self.assertEqual(chmd.id, "bar")
        self.assertEqual(chmd.link, "/episodes/01/03/")
        self.assertEqual(chmd.extra, episode)


class TestComparisonData(TestCase):
    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def test_ctor(self):
        cmp = charts.ComparisonData(self.episodes, self.characters, None)
        self.assertIsInstance(cmp.episodes, QuerySet)
        self.assertListEqual(list(cmp.episodes), self.episodes)

    def test_keys(self):
        cmp_ref = charts.ComparisonData(self.episodes, self.characters, None, 0)
        cmp_ep = charts.ComparisonData(self.episodes[:2], self.characters, None, 0)
        cmp_chars = charts.ComparisonData(self.episodes, self.characters[:2], None, 0)
        cmp_other = charts.ComparisonData(self.episodes, self.characters, self.characters[2:], 0)
        cmp_metric = charts.ComparisonData(self.episodes, self.characters, None, 1)
        self.assertTrue(cmp_ref.key().startswith("comparison"))
        self.assertNotEqual(cmp_ref.key(), cmp_ep.key())
        self.assertNotEqual(cmp_ref.key(), cmp_chars.key())
        self.assertNotEqual(cmp_ref.key(), cmp_other.key())
        self.assertNotEqual(cmp_ref.key(), cmp_metric.key())

    def test_evaluate_lines_no_other(self):
        cmp = charts.ComparisonData(self.episodes, self.characters, None)
        chart_data = cmp.evaluate()
        self.assertEquals(
            [vars(x) for x in chart_data.records],
            [vars(charts.episode_metadata(x)) for x in self.episodes]
        )
        self.assertEquals(
            [vars(x) for x in chart_data.items],
            [vars(charts.character_metadata(x)) for x in self.characters]
        )
        self.assertEquals(
            chart_data.values,
            [
                [15, 7, 3],
                [4, 0, 10],
                [0, 0, 0],
                [7, 8, 9],
            ]
        )

    def test_evaluate_lines_other(self):
        characters = [self.characters[0]]
        others = self.characters[1:]
        cmp = charts.ComparisonData(
            self.episodes, characters, others)
        chart_data = cmp.evaluate()
        self.assertEquals(
            [vars(x) for x in chart_data.records],
            [vars(charts.episode_metadata(x)) for x in self.episodes]
        )
        self.assertEquals(
            [vars(x) for x in chart_data.items],
            [vars(charts.character_metadata(x)) for x in characters] +
                [vars(charts.charts.MetaData("Other", "other"))]
        )
        self.assertEquals(
            chart_data.values,
            [
                [15, 7+3],
                [ 4, 0+10],
                [ 0, 0+0],
                [ 7, 8+9],
            ]
        )

    def test_evaluate_cumlines_no_other(self):
        cmp = charts.ComparisonData(self.episodes, self.characters, None,
                                    charts.ComparisonData.CumultativeLines)
        chart_data = cmp.evaluate()
        self.assertEquals(
            [vars(x) for x in chart_data.records],
            [vars(charts.episode_metadata(x)) for x in self.episodes]
        )
        self.assertEquals(
            [vars(x) for x in chart_data.items],
            [vars(charts.character_metadata(x)) for x in self.characters]
        )
        self.assertEquals(
            chart_data.values,
            [
                [15,       7,       3       ],
                [15+4,     7+0,     3+10    ],
                [15+4+0,   7+0+0,   3+10+0  ],
                [15+4+0+7, 7+0+0+8, 3+10+0+9],
            ]
        )

    def test_evaluate_cumlines_other(self):
        characters = [self.characters[0]]
        others = self.characters[1:]
        cmp = charts.ComparisonData(
            self.episodes, characters, others,
            charts.ComparisonData.CumultativeLines
        )
        chart_data = cmp.evaluate()
        self.assertEquals(
            [vars(x) for x in chart_data.records],
            [vars(charts.episode_metadata(x)) for x in self.episodes]
        )
        self.assertEquals(
            [vars(x) for x in chart_data.items],
            [vars(charts.character_metadata(x)) for x in characters] +
                [vars(charts.charts.MetaData("Other", "other"))]
        )
        self.assertEquals(
            chart_data.values,
            [
                [15,             (7)+(3)       ],
                [15+4,         (7+0)+(3+10)    ],
                [15+4+0,     (7+0+0)+(3+10+0)  ],
                [15+4+0+7, (7+0+0+8)+(3+10+0+9)],
            ]
        )

    def test_evaluate_cumepisodes_no_other(self):
        cmp = charts.ComparisonData(
            self.episodes, self.characters, None,
            charts.ComparisonData.EpisodeCount
        )
        chart_data = cmp.evaluate()
        self.assertEquals(
            [vars(x) for x in chart_data.records],
            [vars(charts.episode_metadata(x)) for x in self.episodes]
        )
        self.assertEquals(
            [vars(x) for x in chart_data.items],
            [vars(charts.character_metadata(x)) for x in self.characters]
        )
        self.assertEquals(
            chart_data.values,
            [
                [1, 1, 1],
                [2, 1, 2],
                [2, 1, 2],
                [3, 2, 3],
            ]
        )

    def test_evaluate_cumepisodes_other(self):
        characters = [self.characters[0]]
        others = self.characters[1:]
        cmp = charts.ComparisonData(
            self.episodes, characters, others,
            charts.ComparisonData.EpisodeCount
        )
        chart_data = cmp.evaluate()
        self.assertEquals(
            [vars(x) for x in chart_data.records],
            [vars(charts.episode_metadata(x)) for x in self.episodes]
        )
        self.assertEquals(
            [vars(x) for x in chart_data.items],
            [vars(charts.character_metadata(x)) for x in characters] +
                [vars(charts.charts.MetaData("Other", "other"))]
        )
        self.assertEquals(
            chart_data.values,
            [
                [1, 1],
                [2, 2],
                [2, 2],
                [3, 3],
            ]
        )

    def test_evaluate_mainchar_no_other(self):
        cmp = charts.ComparisonData(
            self.episodes, self.characters, None,
            charts.ComparisonData.MainCharacter
        )
        chart_data = cmp.evaluate()
        self.assertEquals(
            [vars(x) for x in chart_data.records],
            [vars(charts.episode_metadata(x)) for x in self.episodes]
        )
        self.assertEquals(
            [vars(x) for x in chart_data.items],
            [vars(charts.character_metadata(x)) for x in self.characters]
        )
        self.assertEquals(
            chart_data.values,
            [
                [1, 0, 0],
                [1, 0, 1],
                [1, 0, 1],
                [2, 1, 2],
            ]
        )

    def test_evaluate_mainchar_other(self):
        characters = [self.characters[0]]
        others = self.characters[1:]
        cmp = charts.ComparisonData(
            self.episodes, characters, others,
            charts.ComparisonData.MainCharacter
        )
        chart_data = cmp.evaluate()
        self.assertEquals(
            [vars(x) for x in chart_data.records],
            [vars(charts.episode_metadata(x)) for x in self.episodes]
        )
        self.assertEquals(
            [vars(x) for x in chart_data.items],
            [vars(charts.character_metadata(x)) for x in characters] +
                [vars(charts.charts.MetaData("Other", "other"))]
        )
        self.assertEquals(
            chart_data.values,
            [
                [1, 0],
                [1, 1],
                [1, 1],
                [2, 2],
            ]
        )

    def test_query_string(self):
        characters = [self.characters[0]]
        others = self.characters[1:]
        cmp = charts.ComparisonData(
            self.episodes, characters, others,
            charts.ComparisonData.EpisodeCount
        )
        self.assertEqual(
            cmp.query_string(),
            "episodes=101&episodes=102&episodes=103&episodes=104&"
            "other=2&other=3&"
            "metric=2&"
            "characters=1&"
            .rstrip("&")
        )

    def test_data(self):
        cmp = charts.ComparisonData(self.episodes, self.characters, None)
        patched_cache = LocMemCache('TestComparisonData', {})
        patched_cache.clear()
        with patch.object(charts, "cache", patched_cache):
            chart_data = cmp.data()
            self.assertEquals(
                chart_data.values,
                [
                    [15, 7, 3],
                    [4, 0, 10],
                    [0, 0, 0],
                    [7, 8, 9],
                ]
            )

            cached_data = patched_cache.get(cmp.key())
            self.assertEquals(
                [vars(x) for x in chart_data.records],
                [vars(x) for x in cached_data.records],
            )
            self.assertEquals(
                [vars(x) for x in chart_data.items],
                [vars(x) for x in cached_data.items],
            )
            self.assertEquals(chart_data.values, cached_data.values)

            with patch.object(cmp, "evaluate") as evaluate:
                chart_data = cmp.data()
                self.assertEquals(chart_data.values, cached_data.values)
                evaluate.assert_not_called()


class TestChartRendering(TestCase):
    svg_width = 30.0
    svg_height = 15.0
    svg_chart_type = "line_chart"
    args_dict = QueryDict(mutable=True)
    args_dict.update({
        "width": svg_width,
        "height": svg_height,
        "chart_type": svg_chart_type,
        "padding": 0,
        "nocss": 1,
    })
    args_dict.setlist("characters", range(1, 5))
    args_dict.setlist("episodes", range(101, 105))
    namespaces = {
        "svg":   "http://www.w3.org/2000/svg",
        "xlink": "http://www.w3.org/1999/xlink",
        "cc":    "http://creativecommons.org/ns#",
        "rdf":   "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "dc":    "http://purl.org/dc/elements/1.1/",
    }

    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def assert_correct_svg(self, svg, other=False):
        root = ElementTree.fromstring(svg)
        tree = ElementTree.ElementTree(root)

        self.assert_xml_tag(root, "svg", "svg")
        self.assert_xml_has_attributes(root, {
            "width": str(self.svg_width),
            "height": str(self.svg_height),
            "class": self.svg_chart_type,
        })
        self.assert_xml_node_exists(tree, ".//cc:license[@rdf:resource='AGPLv3+']")

        c0 = tree.find(".//svg:path[@class='character_ch_0']", self.namespaces)
        self.assertEqual(c0.attrib["d"], "M 0.0,0.0 L 10.0,11.0 20.0,15.0 30.0,8.0")

        c1 = tree.find(".//svg:path[@class='character_ch_1']", self.namespaces)
        self.assertEqual(c1.attrib["d"], "M 0.0,8.0 L 10.0,15.0 20.0,15.0 30.0,7.0")

        c2id = "character_other" if other else "character_ch_2"
        c2 = tree.find(".//svg:path[@class='%s']" % c2id, self.namespaces)
        self.assertEqual(c2.attrib["d"], "M 0.0,12.0 L 10.0,5.0 20.0,15.0 30.0,6.0")

    def assert_xml_tag(self, element, tag, ns=None):
        if ns:
            if ns in self.namespaces:
                ns = self.namespaces[ns]
            tag = "{%s}%s" % (ns, tag)
        self.assertEqual(element.tag, tag)

    def assert_xml_has_attributes(self, element, attributes):
        self.assertDictContainsSubset(attributes, element.attrib)

    def assert_xml_node_exists(self, tree, xpath):
        self.assertIsNotNone(
            tree.find(xpath, self.namespaces),
            "Could not find node: %s" % xpath
        )

    def test_render_chart_from_args(self):
        cmp = charts.ComparisonData(self.episodes, self.characters, None)
        chart_data = cmp.data()
        svg = charts.render_chart_from_args(
            self.svg_chart_type, chart_data, self.args_dict
        )
        self.assert_correct_svg(svg)

    def test_chart_view_ok(self):
        request = RequestFactory().get("/?" + self.args_dict.urlencode())
        resp = charts.chart_view(request, self.svg_chart_type)
        self.assertEqual(resp['Content-Type'], "image/svg+xml")
        self.assertEqual(resp.status_code, 200)
        self.assert_correct_svg(resp.content)

    def test_get_chart_view(self):
        request = RequestFactory().get("/?" + self.args_dict.urlencode())
        resp = charts.get_chart_view(request)
        self.assertEqual(resp['Content-Type'], "image/svg+xml")
        self.assertEqual(resp.status_code, 200)
        self.assert_correct_svg(resp.content)

    def assert_error_response(self, resp, code=400):
        self.assertEqual(resp.status_code, code)
        self.assertEqual(resp['Content-Type'], "text/plain")

    def test_chart_view_wrong_type(self):
        request = RequestFactory().get("/?" + self.args_dict.urlencode())
        resp = charts.chart_view(request, "-test_chart_view_wrong_type-")
        self.assert_error_response(resp)

    def test_chart_view_too_many(self):
        args_dict = self.args_dict.copy()
        args_dict.setlist("characters", range(charts.max_characters + 3))
        request = RequestFactory().get("/?" + args_dict.urlencode())
        resp = charts.chart_view(request, self.svg_chart_type)
        self.assert_error_response(resp)

    def test_chart_view_too_few(self):
        args_dict = self.args_dict.copy()
        args_dict.setlist("characters", [])
        request = RequestFactory().get("/?" + args_dict.urlencode())
        resp = charts.chart_view(request, self.svg_chart_type)
        self.assert_error_response(resp)

    def test_chart_view_wrong_episodes(self):
        args_dict = self.args_dict.copy()
        args_dict.setlist("episodes", [5])
        request = RequestFactory().get("/?" + args_dict.urlencode())
        resp = charts.chart_view(request, self.svg_chart_type)
        self.assert_error_response(resp)

    def test_chart_view_all_episodes(self):
        args_dict = self.args_dict.copy()
        del args_dict["episodes"]
        request = RequestFactory().get("/?" + args_dict.urlencode())
        resp = charts.chart_view(request, self.svg_chart_type)
        self.assertEqual(resp['Content-Type'], "image/svg+xml")
        self.assertEqual(resp.status_code, 200)
        self.assert_correct_svg(resp.content)

    def test_chart_view_other(self):
        args_dict = self.args_dict.copy()
        args_dict.setlist("characters", [1, 2])
        args_dict.setlist("other", [3])
        request = RequestFactory().get("/?" + args_dict.urlencode())
        resp = charts.chart_view(request, self.svg_chart_type)
        self.assertEqual(resp['Content-Type'], "image/svg+xml")
        self.assertEqual(resp.status_code, 200)
        self.assert_correct_svg(resp.content, True)

    def test_chart_view_raises(self):
        def throw(*args):
            raise ValueError("Test")
        request = RequestFactory().get("/?" + self.args_dict.urlencode())
        with patch.object(request.GET, "getlist", side_effect=throw):
            with self.settings(DEBUG=False):
                resp = charts.chart_view(request, self.svg_chart_type)
                self.assert_error_response(resp)
            with self.settings(DEBUG=True):
                self.assertRaisesMessage(ValueError, "Test",
                    lambda: charts.chart_view(request, self.svg_chart_type))
