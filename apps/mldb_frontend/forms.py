"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

from django import forms
from django.forms import widgets
from django.forms.utils import pretty_name

import haystack.forms
from haystack.utils.highlighting import Highlighter

from ..mldb import models
from .charts import chart_types, ComparisonData


class CheckBoxRenderer(widgets.CheckboxFieldRenderer):
    """
    All of this is needed to have the checkbox outside the label and a class
    on the outer <ul>
    """
    outer_html = '<ul class="multiple_characters_input" {id_attr}>{content}</ul>'


class MultipleModelField(forms.ModelMultipleChoiceField):
    def __init__(self, queryset, min=0, max=None, *args, **kwargs):
        super(MultipleModelField, self).__init__(
            queryset,
            widget=forms.CheckboxSelectMultiple(renderer=CheckBoxRenderer),
            *args,
            **kwargs
        )
        self.min_characters = min
        self.max_characters = max

    def clean(self, value):
        if value is None:
            value = []
        if self.min_characters is not None and len(value) < self.min_characters:
            raise forms.ValidationError(
                "You need to select at least %s." %
                self._pluralize(self.min_characters)
            )
        if self.max_characters is not None and len(value) > self.max_characters:
            raise forms.ValidationError(
                "You can't select more than %s." %
                self._pluralize(self.max_characters)
            )
        return self.queryset.filter(id__in=value)

    def _pluralize(self, number):
        options = self.queryset.model._meta
        return "%s %s" % (
            number,
            options.verbose_name_plural if number != 1 else options.verbose_name
        )


class MultipleCharactersField(MultipleModelField):
    def __init__(self, queryset=None, *args, **kwargs):
        if queryset is None:
            queryset = models.annotate_characters(models.Character.objects.all())
        super(MultipleCharactersField, self).__init__(queryset, *args, **kwargs)


class EpisodeField(forms.ModelChoiceField):
    @staticmethod
    def default_queryset():
        return models.Episode.objects.order_by("id")

    def __init__(self, queryset=None, *args, **kwargs):
        if queryset is None:
            queryset = EpisodeField.default_queryset()
        super(EpisodeField, self).__init__(queryset, *args, **kwargs)

    def to_python(self, value):
        if isinstance(value, self.queryset.model):
            return value
        return super(EpisodeField, self).to_python(value)


class SearchForm(haystack.forms.SearchForm):
    result_count_choices = [10, 25, 100, 200]
    result_count_default = 25

    characters = MultipleCharactersField(required=False)
    results_per_page = forms.TypedChoiceField(
        zip(result_count_choices, result_count_choices),
        coerce=int,
        empty_value=result_count_default,
        initial=result_count_default,
        required=False,
    )

    def __init__(self, data=None, *args, **kwargs):
        if data is not None and "results_per_page" not in data:
            data = data.copy()
            data["results_per_page"] = self.result_count_default
        super(SearchForm, self).__init__(data, *args, **kwargs)

    def search(self):
        if not self.is_valid():
            return self.no_query_found()

        queryset = super(SearchForm, self).search()

        characters = self.cleaned_data["characters"]
        if characters:
            queryset = queryset.filter(characters__in=characters)

        return queryset


class SearchHighlighter(Highlighter):
    """
    Highlighter that doesn't truncate
    """
    def find_window(self, highlight_locations):
        return (0, 9001)


class FormBase(forms.Form):
    error_css_class = 'error_field'

    def __init__(self, data=None, *args, **kwargs):
        if data is not None:
            data = data.copy()
            self_initial = kwargs.get("initial", {})
            for name, field in self.base_fields.iteritems():
                if name not in data:
                    if name in self_initial:
                        data[name] = self_initial[name]
                    elif field.initial is not None:
                        data[name] = field.initial
        super(FormBase, self).__init__(data, *args, **kwargs)

    def as_div(self, elem="div"):
        "Returns this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<' + elem + '%(html_class_attr)s>%(label)s %(field)s%(help_text)s</' + elem + '>',
            error_row='%s',
            row_ender='</' + elem + '>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True)


def metric_field():
    return forms.TypedChoiceField(
        required=False,
        choices=(
            (ComparisonData.EpisodeLines, "Lines per episode"),
            (ComparisonData.CumultativeLines, "Cumulative line count"),
            (ComparisonData.EpisodeCount, "Number of episodes"),
            (ComparisonData.MainCharacter, "Number of episodes being a main character"),
        ),
        initial=ComparisonData.EpisodeLines,
        help_text="Type of data to display",
        coerce=int,
    )


class CompareForm(FormBase):
    characters = MultipleCharactersField(min=2, required=False)
    episode_start = EpisodeField(required=False, label="From")
    episode_finish = EpisodeField(required=False, label="To")
    include_other = forms.BooleanField(required=False, initial=False)
    metric = metric_field()

    def __init__(self, *args, **kwargs):
        kwargs["initial"] = {
            "episode_start": EpisodeField.default_queryset().first(),
            "episode_finish": EpisodeField.default_queryset().last(),
        }
        super(CompareForm, self).__init__(*args, **kwargs)

    def clean(self):
        data = super(CompareForm, self).clean()
        if "episode_start" in data and "episode_finish" in data:
            episode_start = data["episode_start"]
            episode_finish = data["episode_finish"]

            if episode_finish.id < episode_start.id:
                temp = episode_start
                episode_start = episode_finish
                episode_finish = temp

            data["episode_range"] = models.Episode.objects.filter(
                id__gte=episode_start.id,
                id__lte=episode_finish.id,
            )
        return data


class ChartFormData(FormBase):
    characters = MultipleCharactersField(min=0, max=None)
    episodes = MultipleCharactersField(
        EpisodeField.default_queryset(), min=0, max=None, required=False)
    other = forms.BooleanField(
        required=False,
        label="Include other",
        help_text="Whether there should be an extra entry showing aggregate data for excluded characters"
    )
    metric = metric_field()


class ChartFormGlobalStyle(FormBase):
    chart_type = forms.ChoiceField(tuple((x, pretty_name(x)) for x in chart_types))
    width = forms.IntegerField(min_value=1, initial=600, required=False)
    height = forms.IntegerField(min_value=1, initial=300, required=False)
    padding = forms.IntegerField(
        initial=6,
        required=False,
        help_text="Space between the edge of the image and the data points",

    )
    include_css = forms.BooleanField(
        initial=True,
        required=False,
        help_text="Whether it should include style definitions for styling data based on the character",
    )


class ChartFormExtraStyle(FormBase):
    normalized = forms.BooleanField(
        required=False,
        help_text="For chart types supporting this, expands values to represent percentages, "
                  "thus making them fill the whole height of the chart",
    )
    point_radius = forms.IntegerField(
        min_value=0, initial=4, required=False,
        help_text="For line charts, the radius of the circle used to represent data points",
    )
    separation = forms.FloatField(
        min_value=0, required=False, initial=1.0,
        help_text="For bar charts, distance between bars (as a fraction of the bar width)",
    )
    angle_start = forms.IntegerField(
        min_value=0, max_value=360, required=False, initial=0,
        label="Starting angle",
        help_text="For pie charts, angle of the line between the first and the last element.",
    )
