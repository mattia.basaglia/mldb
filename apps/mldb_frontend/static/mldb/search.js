/*
Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var checked_class = "checked";

function toggle_checkbox_class(checkbox)
{
    var class_list = checkbox.parentNode.parentNode.classList;
    if ( checkbox.checked )
        class_list.add(checked_class);
    else
        class_list.remove(checked_class);
}

function for_each(seq, func)
{
    Array.prototype.forEach.call(seq, func);
}

window.addEventListener("load", function(){
    for_each(
        document.getElementsByClassName("multiple_characters_input"),
        function(element) {
            for_each(
                element.getElementsByTagName("input"),
                function(input) {
                    input.addEventListener(
                        "click",
                        function(ev){ toggle_checkbox_class(this); }
                    )
                    toggle_checkbox_class(input);
                    input.parentNode.parentNode.addEventListener(
                        "click",
                        function(ev){
                            if ( ev.target == this.parentNode.parentNode )
                                this.click();
                        }.bind(input)
                    )
                }
            )
        }
    )
})
