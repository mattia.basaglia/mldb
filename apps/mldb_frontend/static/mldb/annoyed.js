/*
Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \brief This class sets up all the required actions for the boiling rage animation
 * \param element_top       DOM element that when hovered is hidden and triggers
 *                          the animation
 * \param element_bottom    DOM element that gets animated
 */
function Annoy(element_top, element_bottom)
{
    /**
     * \brief Returns a random coordinate for the given maximum offset amount
     */
    this.shake_coord = function(amount)
    {
        return (Math.random() * (amount*2) - amount);
    };

    /**
     * \brief Moves \p element_bottom to the given coordinates
     */
    this.move = function(x, y)
    {
        this.element_bottom.style.left = x + "px";
        this.element_bottom.style.top = y + "px";
    };

    /**
     * \brief Handles opacity of \p element_top and the position of \p element_bottom
     */
    this.shake = function()
    {
        if ( this.annoyed )
        {
            if ( this.annoyance <= this.max_annoyance - this.annoyance_step_up )
                this.annoyance += this.annoyance_step_up;
            else
                this.annoyance = this.max_annoyance;
            this.move(this.shake_coord(this.annoyance), this.shake_coord(this.annoyance));
            element_top.style.opacity = 0;
            this.start();
        }
        else
        {
            this.annoyance = 0;
            element_top.style.opacity = 1;
            this.move(0, 0);
        }
    };

    /**
     * \brief Starts the timer for the next frame
     */
    this.start = function()
    {
        window.setTimeout(this.bound_shake, 10);
    };

    /**
     * \brief Sets up an event listener that changes the annoyed status
     * \param name  Event name
     * \param value Value for annoyed
     */
    this.annoy_event = function(name, value)
    {
        this.element_top.addEventListener(
            name,
            function(event) {
                this.annoyed = value;
                if ( value )
                    this.start();
            }.bind(this),
            false
        );
    };

    /// Top element
    this.element_top = element_top;
    /// Bottom element
    this.element_bottom = element_bottom;
    /// Maximum offset \p element_bottom can be moved by
    this.max_annoyance = 5;
    /// Current annoyance factor
    this.annoyance = 0;
    /// How much to increate annoyance by each frame
    this.annoyance_step_up = 0.01;
    /// Whether annoyance should increase or not
    this.annoyed = false;
    /// Bound version of shake to pass to the timeout function
    this.bound_shake = function(){ this.shake(); }.bind(this);

    this.annoy_event("touchstart", true);
    this.annoy_event("touchend",   false);
    this.annoy_event("mouseenter", true);
    this.annoy_event("mouseleave", false);
}

function set_up_annoy(wrapper)
{
    return new Annoy(wrapper.firstElementChild, wrapper.lastElementChild)
}

function find_all_annoy(class_name)
{
    var wrappers = document.getElementsByClassName(class_name);
    for ( var i = 0; i < wrappers.length; i++ )
        set_up_annoy(wrappers[i]);
}
