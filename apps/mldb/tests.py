"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os

from mock import MagicMock, patch

from django.test import TestCase
from django.test.client import RequestFactory
from django.core.management import call_command
from django.core.management.base import CommandError
from django.utils.six import StringIO
from django.utils import timezone

from . import models
from . import admin
from . import data
from . import search_indexes as search


def setup_line(character, episode, count):
    order_start = episode.line_set.count()
    for i in xrange(count):
        order = order_start + i
        line = models.Line(text="foo", order=order, episode=episode)
        line.save()
        line.characters.add(character)


def setup_lines(cls):
    cls.characters = [
        models.Character(name="Ch%s" % i, slug="ch_%s" % i)
        for i in xrange(3)
    ]
    for character in cls.characters:
        character.save()

    cls.episodes = [
        models.Episode(id=100 + i, title="Ep%s" % i, slug="ep_%s" % i)
        for i in xrange(1, 5)
    ]
    for episode in cls.episodes:
        episode.save()

    setup_line(cls.characters[0], cls.episodes[0], 15)
    setup_line(cls.characters[1], cls.episodes[0], 7)
    setup_line(cls.characters[2], cls.episodes[0], 3)

    setup_line(cls.characters[0], cls.episodes[1], 4)
    setup_line(cls.characters[2], cls.episodes[1], 10)

    setup_line(cls.characters[0], cls.episodes[3], 7)
    setup_line(cls.characters[1], cls.episodes[3], 8)
    setup_line(cls.characters[2], cls.episodes[3], 9)


class TestEpisode(TestCase):
    @classmethod
    def setUpTestData(cls):
        models.Episode.objects.bulk_create([
            models.Episode(301, "bar_foo", "Bar Foo"),
            models.Episode(204, "foo_bar_2", "Foo Bar 2"),
            models.Episode(203, "foo_bar_1", "Foo Bar 1"),
        ])

    def test_season(self):
        episode = models.Episode(203, "foo_bar", "Foo Bar")
        self.assertEqual(episode.season, 2)

    def test_number(self):
        episode = models.Episode(203, "foo_bar", "Foo Bar")
        self.assertEqual(episode.number, 3)

    def test_make_id(self):
        self.assertEqual(models.Episode.make_id(3, 5), 305)

    def test_slug_to_title(self):
        self.assertEqual(models.Episode.slug_to_title("Foo"), "Foo")
        self.assertEqual(models.Episode.slug_to_title("Foo_Bar"), "Foo Bar")
        self.assertEqual(models.Episode.slug_to_title("Foo%2B"), "Foo+")
        self.assertEqual(models.Episode.slug_to_title("Foo_(episode)"), "Foo")

    def test_split_id(self):
        self.assertEqual(models.Episode.split_id(305), (3, 5))
        self.assertEqual(models.Episode.split_id(325), (3, 25))

    def test_format_id(self):
        self.assertEqual(models.Episode.format_id(305), "03/05")
        episode = models.Episode(203, "foo_bar", "Foo Bar")
        self.assertEqual(episode.formatted_id, "02/03")

    def test_next_previous(self):
        first = models.Episode.objects.get(id=203)
        self.assertIsNone(first.previous)
        self.assertEqual(first.next.id, 204)

        middle = models.Episode.objects.get(id=204)
        self.assertEqual(middle.previous.id, 203)
        self.assertEqual(middle.next.id, 301)

        last = models.Episode.objects.get(id=301)
        self.assertEqual(last.previous.id, 204)
        self.assertIsNone(last.next)

    def test_overall_number(self):
        self.assertEqual(models.Episode.objects.get(id=203).overall_number, 1)
        self.assertEqual(models.Episode.objects.get(id=204).overall_number, 2)
        self.assertEqual(models.Episode.objects.get(id=301).overall_number, 3)

    def test_unicode(self):
        episode = models.Episode(203, "foo_bar", "Foo Bar")
        self.assertEqual(unicode(episode), "02/03 Foo Bar")

    def test_get_or_create(self):
        existing = models.Episode.get_or_create(2, 3, "hello")
        self.assertEqual(existing.slug, "foo_bar_1")

        new = models.Episode.get_or_create(2, 5, "hello")
        self.assertEqual(new.slug, "hello")

    def test_load_line_default_name(self):
        characters = []
        for i in xrange(3):
            characters.append(models.Character(name="Ch%s" % i, slug="ch_%s" % i))
            characters[-1].save()

        lines = [
            (["Ch0"], "hello"),
            (["Ch1", "Ch2"], "world"),
        ]
        episode = models.Episode.objects.first()
        with patch("__builtin__.open") as open, \
             patch.object(models, "parse_lines", return_value=lines):
            before = timezone.now()
            episode.load_lines()
            after = timezone.now()
            self.assertGreaterEqual(episode.last_loaded, before)
            self.assertLessEqual(episode.last_loaded, after)
            open.assert_called_with(os.path.join(data.data_lines_raw, episode.slug))
        self.assertEqual(episode.line_set.count(), 2)

    def test_load_line_explicit_name(self):
        characters = []
        for i in xrange(3):
            characters.append(models.Character(name="Ch%s" % i, slug="ch_%s" % i))
            characters[-1].save()

        lines = [
            (["Ch0"], "hello"),
            (["Ch1", "Ch2"], "world"),
        ]
        episode = models.Episode.objects.first()
        with patch("__builtin__.open") as open, \
             patch.object(models, "parse_lines", return_value=lines):
            before = timezone.now()
            episode.load_lines("foo123")
            after = timezone.now()
            self.assertGreaterEqual(episode.last_loaded, before)
            self.assertLessEqual(episode.last_loaded, after)
            open.assert_called_with("foo123")
        self.assertEqual(episode.line_set.count(), 2)

    def test_line_unicode(self):
        self.assertEqual(unicode(models.Line(text="Foo bar")), "Foo bar")

    def test_main_characters(self):
        episode = models.Episode.objects.first()
        characters = [
            models.Character(name="Ch%s" % i, slug="ch_%s" % i)
            for i in xrange(3)
        ]
        for character in characters:
            character.save()

        setup_line(characters[0], episode, 15)
        setup_line(characters[1], episode, 8)
        setup_line(characters[2], episode, 3)

        self.assertListEqual(characters[:1], episode.main_characters())
        self.assertListEqual(characters[:2], episode.main_characters(0.5))
        self.assertListEqual(characters, episode.main_characters(0))

    def test_main_characters_single(self):
        episode = models.Episode.objects.first()
        characters = [
            models.Character(name="Ch%s" % i, slug="ch_%s" % i)
            for i in xrange(3)
        ]
        for character in characters:
            character.save()

        setup_line(characters[0], episode, 15)

        self.assertListEqual(characters[:1], episode.main_characters(0))


class TestCharacter(TestCase):
    def test_name_to_slug(self):
        self.assertEqual(models.Character.name_to_slug("Foo"), "foo")
        self.assertEqual(models.Character.name_to_slug("Foo Bar"), "foo_bar")
        self.assertEqual(models.Character.name_to_slug("Fo\xc3\xb6"), "foo")
        self.assertEqual(models.Character.name_to_slug("Fo0++"), "fo0")

    def test_get_or_create_all(self):
        bar = models.Character(1, "Bar", "bar")
        bar.save()

        foobar = models.CharacterAlias(1, "Foobar")
        foobar.save()

        foo = models.Character(2, "Foo", "foo")
        foo.save()
        foo.aliases.add(foobar)

        self.assertEqual(
            models.Character.get_or_create_all(["Foo"]),
            set([foo])
        )

        self.assertEqual(
            models.Character.get_or_create_all(["Foobar"]),
            set([foo])
        )

        self.assertEqual(
            models.Character.get_or_create_all(["Foo", "Bar", "Foobar"]),
            set([foo, bar])
        )

        new = list(models.Character.get_or_create_all(["Fooey"]))
        self.assertEqual(len(new), 1)
        self.assertIsInstance(new[0], models.Character)
        self.assertNotIn(new[0], [foo, bar])
        self.assertEqual(new[0].name, "Fooey")

    def test_annotate(self):
        setup_lines(self)
        query_set = models.annotate_characters(models.Character.objects.all())
        self.assertListEqual(
            list(query_set.values("id", "n_lines", "episodes")),
            [
                {
                    "id": 1,
                    "n_lines": 15+4+7,
                    "episodes": 3,
                },
                {
                    "id": 3,
                    "n_lines": 3+10+9,
                    "episodes": 3,
                },
                {
                    "id": 2,
                    "n_lines": 7+8,
                    "episodes": 2,
                },
            ]
        )

    def test_character_alias(self):
        character_alias = models.CharacterAlias(name="name")
        character_alias.save()

        characters = []
        for i in xrange(3):
            characters.append(models.Character(name="Ch%s" % i, slug="ch_%s" % i))
            characters[-1].save()
            characters[-1].aliases.add(character_alias)

        self.assertEqual(unicode(character_alias), "name -> Ch0, Ch1, Ch2")

    def test_unicode(self):
        self.assertEqual(unicode(models.Character(name="Foo")), "Foo")


class TestAdmin(TestCase):
    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def get_admin(self, model):
        return admin.admin.site._registry[model]

    def test_character_get_queryset(self):
        user_admin = self.get_admin(models.Character)
        queryset = user_admin.get_queryset(RequestFactory().get(""))
        self.assertListEqual(list(queryset.values_list(flat=True)), [1, 3, 2])
        self.assertEqual(queryset[2].episodes, 2)

    def test_color_field(self):
        user_admin = self.get_admin(models.Character)
        self.assertIn("color_field", user_admin.list_display)
        character = models.Character(color='#123', outline='#456')
        html = user_admin.color_field(character)
        self.assertIn("background: #123", html)
        self.assertRegexpMatches(html, "border:.*#456")

    def make_alias(self, name, character_indices):
        character_alias = models.CharacterAlias(name=name)
        character_alias.save()
        for index in character_indices:
            self.characters[index].aliases.add(character_alias)
        return character_alias

    def test_character_alias_admin_form_commit(self):
        character_alias = self.make_alias("foo", [0])
        self.characters[0].aliases.add(character_alias)
        form = admin.CharacterAliasAdminForm(
            {"name": "bar", "characters": [2]},
            instance=character_alias
        )
        self.assertListEqual(
            list(form.fields['characters'].initial.values_list("id", flat=True)),
            [1]
        )

        saved = form.save(True)
        self.assertEqual(saved.name, "bar")
        self.assertListEqual(
            list(saved.character_set.values_list("id", flat=True)),
            [2]
        )

        character_alias.refresh_from_db()
        self.assertEqual(character_alias.name, "bar")
        self.assertListEqual(
            list(character_alias.character_set.values_list("id", flat=True)),
            [2]
        )

    def test_character_alias_admin_form_nocommit(self):
        character_alias = self.make_alias("foo", [0])
        form = admin.CharacterAliasAdminForm(
            {"name": "bar", "characters": [2]},
            instance=character_alias
        )
        self.assertListEqual(
            list(form.fields['characters'].initial.values_list("id", flat=True)),
            [1]
        )

        saved = form.save(False)
        self.assertEqual(saved.name, "bar")

        character_alias = models.CharacterAlias.objects.get()
        self.assertEqual(character_alias.name, "foo")
        self.assertListEqual(
            list(character_alias.character_set.values_list("id", flat=True)),
            [1]
        )

        saved.save()
        form.save_m2m()
        character_alias.refresh_from_db()
        self.assertEqual(character_alias.name, "bar")
        self.assertListEqual(
            list(character_alias.character_set.values_list("id", flat=True)),
            [2]
        )

    def test_has_reverse_key_filter(self):
        hrkf_class = admin.has_reverse_key_filter("character")
        self.assertEqual(hrkf_class.parameter_name, "character")
        request = RequestFactory().get("")
        model = models.CharacterAlias
        admin_obj = self.get_admin(model)
        list_filter = hrkf_class(request, {}, model, admin_obj)

        lookups = list_filter.lookups(request, admin_obj)
        self.assertSetEqual({l[0] for l in lookups}, {1, 0})

        self.make_alias("foo", [0])
        self.make_alias("bar", [1, 2])
        self.make_alias("baz", [])
        queryset = model.objects.all()

        self.assertEqual(len(list_filter.queryset(request, queryset)), 3)

        list_filter.used_parameters["character"] = 1
        self.assertEqual(len(list_filter.queryset(request, queryset)), 2)

        list_filter.used_parameters["character"] = 0
        self.assertEqual(len(list_filter.queryset(request, queryset)), 1)

        list_filter.used_parameters["character"] = 69
        self.assertEqual(len(list_filter.queryset(request, queryset)), 3)

    def test_episode_admin(self):
        admin_obj = self.get_admin(models.Episode)
        request = RequestFactory().get("")
        request.user = MagicMock()
        queryset = models.Episode.objects.filter(id=101)

        request.user.has_perms.return_value = False
        self.assertRaises(
            admin.PermissionDenied,
            lambda: admin_obj.reload_lines(request, queryset)
        )
        self.assertRaises(
            admin.PermissionDenied,
            lambda: admin_obj.bump_last_loaded(request, queryset)
        )

        admin_obj.message_user = MagicMock()
        request.user.has_perms.return_value = True

        with patch.object(admin.models.Episode, "load_lines"):
            admin_obj.reload_lines(request, queryset)
            self.assertFalse(models.Episode.objects.get(id=101).line_set.all())
            admin_obj.message_user.assert_called()

        before = timezone.now()
        admin_obj.bump_last_loaded(request, queryset)
        after = timezone.now()
        last_loaded = models.Episode.objects.get(id=101).last_loaded
        self.assertGreaterEqual(last_loaded, before)
        self.assertLessEqual(last_loaded, after)
        admin_obj.message_user.assert_called()

        episode = models.Episode.objects.get(id=101)
        self.assertEqual(admin_obj.formatted_id(episode), episode.formatted_id)


class TestWikiTranscript(TestCase):
    def test_markov_case(self):
        output = []
        base_path = os.path.join(data.data_lines_root, "test")
        with open(os.path.join(base_path, "transcript_raw")) as transcript_raw:
            for names, line in data.parse_lines(transcript_raw):
                for name in names:
                    output.append("%s:::%s\n" % (name, line))

        with open(os.path.join(base_path, "transcript_parsed")) as parsed:
            self.assertListEqual(list(parsed), output)


class TestCommands(TestCase):
    @classmethod
    def setUpTestData(cls):
        setup_lines(cls)

    def test_alias_character_ok(self):
        stdin = ["Foo\n", "Bar\n"]
        with patch("sys.stdin", stdin):
            call_command('alias_character', names=["Ch0"])
        self.assertListEqual(
            list(models.CharacterAlias.objects.all().values_list("name", flat=True)),
            ["Foo", "Bar"]
        )
        self.assertListEqual(
            list(self.characters[0].aliases.values_list("name", flat=True)),
            ["Foo", "Bar"]
        )

    def test_alias_character_error(self):
        stdin = ["Foo\n", "Bar\n"]
        with patch("sys.stdin", stdin):
            self.assertRaises(
                CommandError,
                lambda: call_command('alias_character', names=["Ch20"])
            )
        self.assertEqual(models.CharacterAlias.objects.count(), 0)

    def test_clean_aliases_nokeep(self):
        alias = models.CharacterAlias(name="Ch0")
        alias.save()
        self.characters[1].aliases.add(alias)
        stdout = StringIO()
        call_command('clean_aliases', stdout=stdout)
        self.assertTrue(
            models.Line.objects
                .filter(episode=102, characters__in=[self.characters[1]])
                .count(),
            4
        )
        self.assertTrue(
            models.Character.objects.count(),
            2
        )
        self.assertMultiLineEqual(
            "Converting %s lines\nAliased characters:\nCh0\nCleaning them up...\n"
            % (15+4+7),
            stdout.getvalue()
        )

    def test_similar_characters(self):
        models.Character(name="Foobar").save()
        stdout = StringIO()
        call_command('similar_characters', "Ch0", stdout=stdout)
        self.assertSetEqual(
            set(stdout.getvalue().splitlines()),
            {"Ch0", "Ch1", "Ch2"}
        )
        call_command('similar_characters', "Ch0", stdout=stdout, threshold=0)
        self.assertSetEqual(
            set(stdout.getvalue().splitlines()),
            {"Ch0", "Ch1", "Ch2", "Foobar"}
        )

    def test_load_episode_list(self):
        eplist = ["Foo_1", "Foo_2", "Bar"]
        stdout = StringIO()
        with patch("os.listdir", return_value=eplist) as listdir:
            call_command("load_episode", list=True, stdout=stdout)
            self.assertListEqual(
                stdout.getvalue().splitlines(),
                eplist
            )
            listdir.assert_called_with(data.data_lines_raw)

    def test_load_episode_no_action(self):
        self.assertRaises(CommandError, lambda: call_command("load_episode"))

    def test_load_episode_missing_arguments(self):
        self.assertRaises(
            CommandError,
            lambda: call_command("load_episode", slug="Foo_2", episode=1)
        )
        self.assertRaises(
            CommandError,
            lambda: call_command("load_episode", slug="Foo_2", season=2)
        )

    def test_load_episode_new(self):
        with patch("apps.mldb.models.Episode.get_or_create") as get_or_create:
            episode = MagicMock()
            episode.line_set.exists.return_value = False
            get_or_create.return_value = episode
            call_command("load_episode", slug="Foo_2", episode=1, season=2)
            get_or_create.assert_called_with(2, 1, "Foo_2")
            episode.load_lines.assert_called()

    def test_load_episode_new_id(self):
        with patch("apps.mldb.models.Episode.get_or_create") as get_or_create:
            episode = MagicMock()
            episode.line_set.exists.return_value = False
            get_or_create.return_value = episode
            call_command("load_episode", slug="Foo_2", id=201)
            get_or_create.assert_called_with(2, 1, "Foo_2")
            episode.load_lines.assert_called()

    def test_load_episode_reload_error(self):
        with patch("apps.mldb.models.Episode.get_or_create") as get_or_create:
            episode = MagicMock()
            episode.line_set.exists.return_value = True
            get_or_create.return_value = episode
            self.assertRaises(
                CommandError,
                lambda: call_command("load_episode", slug="Foo_2", episode=1, season=2)
            )
            get_or_create.assert_called_with(2, 1, "Foo_2")
            episode.load_lines.assert_not_called()

    def test_load_episode_reload(self):
        with patch("apps.mldb.models.Episode.get_or_create") as get_or_create:
            episode = MagicMock()
            episode.line_set.exists.return_value = True
            get_or_create.return_value = episode
            call_command("load_episode", slug="Foo_2", episode=1, season=2, reload=True)
            get_or_create.assert_called_with(2, 1, "Foo_2")
            episode.load_lines.assert_called()

    def test_load_episode_reload_id(self):
        with patch("apps.mldb.models.Episode.load_lines", autospec=True) as load_lines:
            call_command("load_episode", id=102, reload=True)
            load_lines.assert_called_with(self.episodes[1])

    def test_load_episode_grep(self):
        eplist = ["Foo_1", "Foo_2", "Bar", "FooBar"]
        input = ["_", "2"]
        input_iter = iter(input)
        with patch("os.listdir", return_value=eplist), \
             patch("apps.mldb.models.Episode.get_or_create") as get_or_create, \
             patch("__builtin__.raw_input", lambda: next(input_iter)):
            episode = MagicMock()
            episode.line_set.exists.return_value = False
            get_or_create.return_value = episode
            stdout = StringIO()
            call_command("load_episode", slug="Foo", id=201, grep=True, stdout=stdout)
            self.assertMultiLineEqual(
                stdout.getvalue(),
                "Foo_1\nFoo_2\nFooBar\n\nFoo_1\nFoo_2\n\nFoo_2\n\nLoading...\n"
            )
            get_or_create.assert_called_with(2, 1, "Foo_2")
            episode.load_lines.assert_called()

    def test_load_episode_grep_not_found(self):
        eplist = ["Foo_1", "Foo_2", "Bar", "FooBar"]
        input = ["_", "3"]
        input_iter = iter(input)
        with patch("os.listdir", return_value=eplist), \
             patch("apps.mldb.models.Episode.get_or_create") as get_or_create, \
             patch("__builtin__.raw_input", lambda: next(input_iter)):
            stdout = StringIO()
            self.assertRaises(
                CommandError,
                lambda: call_command("load_episode", slug="Foo", id=201, grep=True, stdout=stdout)
            )
            self.assertMultiLineEqual(
                stdout.getvalue(),
                "Foo_1\nFoo_2\nFooBar\n\nFoo_1\nFoo_2\n\n\n\n"
            )
            get_or_create.assert_not_called()


class TestLineIndex(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.characters = []
        for i in xrange(3):
            cls.characters.append(models.Character(name="Ch%s" % i, slug="ch_%s" % i))
            cls.characters[-1].save()

        cls.episode = models.Episode(title="Foo", slug="foo", id=101)
        cls.episode.save()

        cls.line1 = models.Line(text="text", episode=cls.episode, order=0)
        cls.line1.save()
        cls.line1.characters = cls.characters

        cls.line2 = models.Line(text="text", episode=cls.episode, order=1)
        cls.line2.save()
        cls.line2.characters = cls.characters[0:1]

    def test_get_model(self):
        self.assertIs(search.LineIndex().get_model(), models.Line)

    def test_prepare_characters(self):
        self.assertListEqual(
            search.LineIndex().prepare_characters(self.line1),
            ["Ch0", "Ch1", "Ch2"]
        )
        self.assertListEqual(
            search.LineIndex().prepare_characters(self.line2),
            ["Ch0"]
        )

    def test_index_queryset(self):
        self.assertListEqual(
            list(search.LineIndex().index_queryset().all()),
            [self.line1, self.line2]
        )
