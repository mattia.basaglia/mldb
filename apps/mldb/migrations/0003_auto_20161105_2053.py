# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-05 20:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mldb', '0002_auto_20161103_2020'),
    ]

    operations = [
        migrations.CreateModel(
            name='CharacterAlias',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=128)),
            ],
            options={
                'verbose_name_plural': 'Character aliases',
            },
        ),
        migrations.AddField(
            model_name='character',
            name='aliases',
            field=models.ManyToManyField(to='mldb.CharacterAlias'),
        ),
    ]
