"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals
import math

from django.test import SimpleTestCase
from django.test.client import RequestFactory

from . import charts
from .templatetags import charts as tt


def make_metadata(name, number):
    return charts.MetaData(
        "%s %d" % (name.capitalize(), number),
        "%s%d" % (name, number),
        "example.com/%s/%d" % (name, number),
        number
    )


def make_data_matrix():
    return charts.DataMatrix(
        [make_metadata("row", i) for i in xrange(4)],
        [make_metadata("column", i) for i in xrange(5)],
        [[i * 10 + j for j in xrange(5)] for i in xrange(4)]
    )


def make_data_set(count=3):
    return charts.DataSet([
        charts.DataPoint(x + 1, "item %s" % x)
        for x in xrange(3)
    ])


class TestMetaData(SimpleTestCase):
    def test_ctor(self):
        self.assertEqual(
            vars(charts.MetaData()),
            {"label": "", "id": "", "link": None, "extra": None}
        )
        md1 = charts.MetaData("foo", "bar", "foo://bar", 123)
        self.assertEqual(
            vars(md1),
            {"label": "foo", "id": "bar", "link": "foo://bar", "extra": 123}
        )
        md2 = charts.MetaData(*md1.ctor_args())
        self.assertEqual(
            vars(md1),
            vars(md2)
        )

    def test_wrap_link_nolink(self):
        self.assertEqual(
            charts.MetaData().wrap_link("foo"),
            "foo"
        )
        self.assertEqual(
            charts.MetaData().wrap_link("<foo/>"),
            "<foo/>"
        )

    def test_wrap_link_norequest(self):
        self.assertEqual(
            charts.MetaData(link="bar").wrap_link("foo"),
            "<a xlink:href='bar'>foo</a>"
        )
        self.assertEqual(
            charts.MetaData(link="<bar/>").wrap_link("<foo/>"),
            "<a xlink:href='&lt;bar/&gt;'><foo/></a>"
        )

    def test_wrap_link_xmlns(self):
        self.assertEqual(
            charts.MetaData(link="bar").wrap_link("foo", "xl"),
            "<a xl:href='bar'>foo</a>"
        )
        self.assertEqual(
            charts.MetaData(link="bar").wrap_link("foo", "xl:"),
            "<a xl:href='bar'>foo</a>"
        )
        self.assertEqual(
            charts.MetaData(link="bar").wrap_link("foo", ""),
            "<a href='bar'>foo</a>"
        )

    def test_wrap_link_request(self):
        request = RequestFactory().get('')

        self.assertEqual(
            charts.MetaData(link="bar").wrap_link("foo", "", request),
            "<a href='http://testserver/bar'>foo</a>",

        )
        self.assertEqual(
            charts.MetaData(link="<bar/>").wrap_link("<foo/>", "", request),
            "<a href='http://testserver/%3Cbar/%3E'><foo/></a>",
        )


class TestDataPoint(SimpleTestCase):
    def test_ctor(self):
        self.assertEqual(
            vars(charts.DataPoint(69)),
            {"label": "", "id": "", "link": None, "extra": None, "value": 69}
        )
        md = charts.MetaData("foo", "bar", "foo://bar", 123)
        mdict = vars(md).copy()
        mdict["value"] = 69
        self.assertEqual(
            vars(charts.DataPoint(69, *md.ctor_args())),
            mdict
        )

    def test_normalized(self):
        val = charts.DataPoint(40)
        self.assertEqual(val.normalized(0), 1.0)
        self.assertEqual(val.normalized(40), 1.0)
        self.assertEqual(val.normalized(100), 0.4)


class TestDataSet(SimpleTestCase):
    def test_max_value(self):
        self.assertEqual(make_data_set().max_value(), 3)
        self.assertEqual(charts.DataSet([]).max_value(), 0)

    def test_total(self):
        self.assertEqual(make_data_set().total(), 6)
        self.assertEqual(charts.DataSet([]).total(), 0)

    def test_append(self):
        dataset = make_data_set()
        dataset.append(charts.DataPoint(4))
        self.assertEqual(dataset.data[-1].value, 4)

    def test_len(self):
        self.assertEqual(len(make_data_set()), 3)

    def test_iter(self):
        dataset = make_data_set()
        self.assertEqual(list(dataset), dataset.data)


class TestMatrixView(SimpleTestCase):
    def test_records(self):
        dm = make_data_matrix()
        self.assertEqual(
            [i.id for i in dm.records],
            ["row%s" % i for i in xrange(4)]
        )

    def test_items(self):
        dm = make_data_matrix()
        self.assertEqual(
            [i.id for i in dm.items],
            ["column%s" % i for i in xrange(5)]
        )

    def test_value(self):
        dm = make_data_matrix()
        self.assertEqual(dm.value(2, 3), 23)

    def test_range(self):
        dm = make_data_matrix()
        self.assertEqual(len(dm.range_records), len(dm.records))
        self.assertEqual(len(dm.range_items), len(dm.items))

    def test_dataset(self):
        dm = make_data_matrix()
        self.assertEqual(
            [x.value for x in dm.record_dataset(1)],
            dm.values[1]
        )
        self.assertEqual(
            [x.value for x in dm.item_dataset(1)],
            [x[1] for x in dm.values]
        )

    def test_datasets(self):
        dv = make_data_matrix()
        self.assertEqual(len(list(dv.record_datasets())), len(dv.records))
        self.assertEqual(
            [x.value for x in list(dv.record_datasets())[0]],
            [x.value for x in dv.record_dataset(0)]
        )
        self.assertEqual(len(list(dv.item_datasets())), len(dv.items))
        self.assertEqual(
            [x.value for x in list(dv.item_datasets())[0]],
            [x.value for x in dv.item_dataset(0)]
        )

    def test_max_value(self):
        self.assertEqual(make_data_matrix().max_value(), 34)

    def test_transposed(self):
        dm = make_data_matrix()
        self.assertIsInstance(dm.transposed, charts.TransposedMatrix)
        self.assertIs(dm.records, dm.transposed.items)
        self.assertIs(dm.items, dm.transposed.records)


class TestTransposedMatrix(SimpleTestCase):
    def test_records(self):
        dm = make_data_matrix()
        self.assertEqual(
            [i.id for i in dm.transposed.records],
            ["column%s" % i for i in xrange(5)]
        )

    def test_items(self):
        dm = make_data_matrix()
        self.assertEqual(
            [i.id for i in dm.transposed.items],
            ["row%s" % i for i in xrange(4)]
        )

    def test_value(self):
        dm = make_data_matrix()
        self.assertEqual(dm.transposed.value(2, 3), 32)

    def test_range(self):
        dm = make_data_matrix()
        self.assertEqual(len(dm.transposed.range_records), len(dm.items))
        self.assertEqual(len(dm.transposed.range_items), len(dm.records))

    def test_dataset(self):
        dm = make_data_matrix()
        self.assertEqual(
            [x.value for x in dm.transposed.record_dataset(1)],
            [x[1] for x in dm.values]
        )
        self.assertEqual(
            [x.value for x in dm.transposed.item_dataset(1)],
            dm.values[1]
        )

    def test_datasets(self):
        dv = make_data_matrix().transposed
        self.assertEqual(len(list(dv.record_datasets())), len(dv.records))
        self.assertEqual(
            [x.value for x in list(dv.record_datasets())[0]],
            [x.value for x in dv.record_dataset(0)]
        )
        self.assertEqual(len(list(dv.item_datasets())), len(dv.items))
        self.assertEqual(
            [x.value for x in list(dv.item_datasets())[0]],
            [x.value for x in dv.item_dataset(0)]
        )

    def test_max_value(self):
        self.assertEqual(make_data_matrix().transposed.max_value(), 34)

    def test_transposed(self):
        dm = make_data_matrix()
        self.assertIsInstance(dm.transposed.transposed, charts.DataMatrix)
        self.assertIs(dm.records, dm.transposed.transposed.records)
        self.assertIs(dm.items, dm.transposed.transposed.items)


class TestMatrixViewSingleRecord(SimpleTestCase):
    record = make_data_set()

    def view(self):
        return charts.MatrixViewSingleRecord(self.record)

    def test_records(self):
        self.assertEqual(self.view().records, [self.record])

    def test_items(self):
        self.assertEqual(self.view().items, self.record)

    def test_value(self):
        self.assertEqual(self.view().value(0, 2), self.record.data[2].value)

    def test_range(self):
        self.assertEqual(len(self.view().range_records), 1)
        self.assertEqual(len(self.view().range_items), len(self.record))

    def test_dataset(self):
        self.assertEqual(self.view().record_dataset(0), self.record)
        self.assertEqual(
            [x.value for x in self.view().item_dataset(1)],
            [self.record[1].value]
        )

    def test_datasets(self):
        dv = self.view()
        self.assertEqual(len(list(dv.record_datasets())), len(dv.records))
        self.assertEqual(
            [x.value for x in list(dv.record_datasets())[0]],
            [x.value for x in dv.record_dataset(0)]
        )
        self.assertEqual(len(list(dv.item_datasets())), len(dv.items))
        self.assertEqual(
            [x.value for x in list(dv.item_datasets())[0]],
            [x.value for x in dv.item_dataset(0)]
        )

    def test_max_value(self):
        self.assertEqual(self.view().max_value(), 3)

    def test_transposed(self):
        dm = self.view()
        self.assertIsInstance(dm.transposed, charts.MatrixViewSingleItem)
        self.assertEqual(dm.records, dm.transposed.items)
        self.assertIs(dm.items, dm.transposed.records)


class TestMatrixViewSingleItem(SimpleTestCase):
    item = make_data_set()

    def view(self):
        return charts.MatrixViewSingleItem(self.item)

    def test_records(self):
        self.assertEqual(self.view().records, self.item)

    def test_items(self):
        self.assertEqual(self.view().items, [self.item])

    def test_value(self):
        self.assertEqual(self.view().value(2, 0), self.item.data[2].value)

    def test_range(self):
        self.assertEqual(len(self.view().range_records), len(self.item))
        self.assertEqual(len(self.view().range_items), 1)

    def test_dataset(self):
        self.assertEqual(
            [x.value for x in self.view().record_dataset(1)],
            [self.item[1].value]
        )
        self.assertEqual(self.view().item_dataset(0), self.item)

    def test_datasets(self):
        dv = self.view()
        self.assertEqual(len(list(dv.record_datasets())), len(dv.records))
        self.assertEqual(
            [x.value for x in list(dv.record_datasets())[0]],
            [x.value for x in dv.record_dataset(0)]
        )
        self.assertEqual(len(list(dv.item_datasets())), len(dv.items))
        self.assertEqual(
            [x.value for x in list(dv.item_datasets())[0]],
            [x.value for x in dv.item_dataset(0)]
        )

    def test_max_value(self):
        self.assertEqual(self.view().max_value(), 3)

    def test_transposed(self):
        dm = self.view()
        self.assertIsInstance(dm.transposed, charts.MatrixViewSingleRecord)
        self.assertIs(dm.records, dm.transposed.items)
        self.assertEqual(dm.items, dm.transposed.records)


class TestSvgRect(SimpleTestCase):
    def test_point(self):
        self.assertEqual(str(charts.SvgPoint(3, 4)), "3,4")
        self.assertEqual(charts.SvgPoint(3, 4), charts.SvgPoint(3.0, 4.0))
        self.assertNotEqual(charts.SvgPoint(3, 5), charts.SvgPoint(3, 4))
        self.assertNotEqual(charts.SvgPoint(5, 4), charts.SvgPoint(3, 4))
        self.assertEqual(repr(charts.SvgPoint(3, 4)), "SvgPoint(3, 4)")

    def test_repr(self):
        self.assertEqual(
            repr(charts.SvgRect(1, 2, 3, 4)),
            "SvgRect(3x4+1+2)"
        )

    def test_test_corners(self):
        self.assertEqual(
            charts.SvgRect(1, 2, 3, 4).top_left,
            charts.SvgPoint(1, 2)
        )
        self.assertEqual(
            charts.SvgRect(1, 2, 3, 4).top_right,
            charts.SvgPoint(1 + 3, 2)
        )
        self.assertEqual(
            charts.SvgRect(1, 2, 3, 4).bottom_right,
            charts.SvgPoint(1 + 3, 2 + 4)
        )
        self.assertEqual(
            charts.SvgRect(1, 2, 3, 4).bottom_left,
            charts.SvgPoint(1, 2 + 4)
        )

    def test_center(self):
        self.assertEqual(
            charts.SvgRect(5, 6, 10, 20).center,
            charts.SvgPoint(10, 16)
        )

    def test_cmp(self):
        self.assertEqual(charts.SvgRect(1, 2, 3, 4), charts.SvgRect(1, 2, 3, 4))
        self.assertNotEqual(charts.SvgRect(1, 2, 3, 4), charts.SvgRect(1, 2, 3, 0))
        self.assertNotEqual(charts.SvgRect(1, 2, 3, 4), charts.SvgRect(1, 2, 0, 4))
        self.assertNotEqual(charts.SvgRect(1, 2, 3, 4), charts.SvgRect(1, 0, 3, 4))
        self.assertNotEqual(charts.SvgRect(1, 2, 3, 4), charts.SvgRect(0, 2, 3, 4))


class TestChartTraits(SimpleTestCase):
    def test_item_samename(self):
        item = charts.TraitsItem("foo", int, 9)
        self.assertEqual(item.get({}), 9)
        self.assertEqual(item.get({"foo": "3"}), 3)
        kw = {}
        item.update_kwargs(kw, {})
        self.assertEqual(kw, {"foo": 9})

    def test_item_altname(self):
        item = charts.TraitsItem("foo", int, 9, "bar")
        self.assertEqual(item.get({}), 9)
        self.assertEqual(item.get({"bar": "3"}), 3)
        kw = {}
        item.update_kwargs(kw, {})
        self.assertEqual(kw, {"foo": 9})

    def test_item_bool(self):
        item = charts.TraitsItem("foo", bool, False)
        self.assertFalse(item.get({}))
        self.assertTrue(item.get({"foo": "3"}))

    def test_get_kwargs(self):
        traits = charts.ChartTraits(
            [
                charts.TraitsItem("hello", int, 3),
                charts.TraitsItem("world", int, 9),
            ],
            [
                charts.TraitsItem("foo", int, 3),
                charts.TraitsItem("bar", int, 9),
            ]
        )
        self.assertEqual(traits.get_ctor_kwargs({}), {"hello": 3, "world": 9})
        self.assertEqual(traits.get_options_kwargs({}), {"foo": 3, "bar": 9})
        self.assertEqual(
            traits.get_options_kwargs({
                "class_prefix": "cls_",
                "attribute_hello": "world",
                "foo": 4,
            }),
            {"foo": 4, "bar": 9, "class_prefix": "cls_", "hello": "world"}
        )


class TestRenderOptions(SimpleTestCase):
    def test_attributes(self):
        ropts = charts.RenderOptions("class_", "id_", None, foo=1, bar=2)

        self.assertEqual(
            ropts.attributes(charts.MetaData()),
            {"foo": 1, "bar": 2}
        )

        self.assertEqual(
            ropts.attributes(charts.MetaData(), bar=3, baz=4),
            {"foo": 1, "bar": 3, "baz": 4}
        )
        self.assertEqual(ropts.attrs, {"foo": 1, "bar": 2})

        self.assertEqual(
            ropts.attributes(charts.MetaData(id="foo")),
            {"foo": 1, "bar": 2, "class": "class_foo", "id": "id_foo"}
        )

        self.assertEqual(
            ropts.attributes(charts.DataPoint(27, "bar", "foo")),
            {"foo": 1, "bar": 2, "class": "class_foo", "id": "id_foo",
             "data-value": 27, "data-name": "bar"}
        )

        ropts.class_prefix = None
        self.assertEqual(
            ropts.attributes(charts.MetaData(id="foo")),
            {"foo": 1, "bar": 2, "id": "id_foo"}
        )

        ropts.class_prefix = "class_"
        ropts.id_prefix = None
        self.assertEqual(
            ropts.attributes(charts.MetaData(id="foo")),
            {"foo": 1, "bar": 2, "class": "class_foo"}
        )

    def test_attribute_string(self):
        ropts = charts.RenderOptions("class_", "id_", None, foo=1, bar=2)
        attribute_string = " " + ropts.attribute_string(charts.MetaData(), baz=3)
        self.assertIn(" foo='1'", attribute_string)
        self.assertIn(" bar='2'", attribute_string)
        self.assertIn(" baz='3'", attribute_string)

    def test_render_element_notitle_nolink(self):
        ropts = charts.RenderOptions(foo=1)
        self.assertEqual(
            ropts.render_element("test", charts.MetaData()),
            "<test foo='1'/>\n"
        )

    def test_render_element_title(self):
        ropts = charts.RenderOptions(foo=1)
        self.assertEqual(
            ropts.render_element("test", charts.MetaData("foo")),
            "<test foo='1'><title>foo</title></test>\n"
        )
        self.assertEqual(
            ropts.render_element("test", charts.MetaData("foo"), "bar"),
            "<test foo='1'><title>bar</title></test>\n"
        )
        self.assertEqual(
            ropts.render_element("test", charts.MetaData("foo&")),
            "<test foo='1'><title>foo&amp;</title></test>\n"
        )

    def test_render_element_link(self):
        ropts = charts.RenderOptions(foo=1)
        self.assertEqual(
            ropts.render_element("test", charts.MetaData(link="bar")),
            "<a xlink:href='bar'><test foo='1'/></a>\n"
        )

        ropts.request = RequestFactory().get('')
        self.assertEqual(
            ropts.render_element("test", charts.MetaData(link="bar")),
            "<a xlink:href='http://testserver/bar'><test foo='1'/></a>\n"
        )


class TestChartBase(SimpleTestCase):
    def test_ctor(self):
        chart = charts.ChartBase(charts.SvgRect(1, 2, 30, 40), 10, False)
        self.assertEqual(chart.rect, charts.SvgRect(11, 12, 10, 20))

    def test_format_title(self):
        chart = charts.ChartBase(charts.SvgRect(0, 0, 0, 0), 0, False)
        point = charts.DataPoint(23, "foo")
        self.assertEqual(chart.format_title(point, 46), "foo (23)")
        chart.normalized = True
        self.assertEqual(chart.format_title(point, 46), "foo (23, 50%)")
        self.assertEqual(chart.format_title(point, 23), "foo (23, 100%)")

    def test_point_rel2abs(self):
        chart = charts.ChartBase(charts.SvgRect(10, 20, 30, 40), 0, False)
        self.assertEqual(
            chart.point_rel2abs(charts.SvgPoint(0.5, 0.25)),
            charts.SvgPoint(25, 50)
        )

    def test_rect_rel2abs(self):
        chart = charts.ChartBase(charts.SvgRect(10, 20, 30, 40), 0, False)
        self.assertEqual(
            chart.rect_rel2abs(charts.SvgRect(0.5, 0.25, 0.25, 0.5)),
            charts.SvgRect(25, 30, 7.5, 20)
        )


class TestPieChart(SimpleTestCase):
    maxDiff = None

    def _check_std_render(self, rendered):
        self.assertEqual(rendered,
            "<path d='M 5.0,5.0 L 10.0,5.0 A 5.0,5.0 0 0 1 7.5,9.33012701892 Z'"
            " data-name='item 0' data-value='1'>"
            "<title>item 0 (1, 17%)</title></path>\n"
            "<path d='M 5.0,5.0 L 7.5,9.33012701892 A 5.0,5.0 0 0 1 0.0,5.0 Z'"
            " data-name='item 1' data-value='2'>"
            "<title>item 1 (2, 33%)</title></path>\n"
            "<path d='M 5.0,5.0 L 0.0,5.0 A 5.0,5.0 0 0 1 10.0,5.0 Z'"
            " data-name='item 2' data-value='3'>"
            "<title>item 2 (3, 50%)</title></path>\n")

    def _check_rotated_render(self, rendered):
        self.assertMultiLineEqual(
            rendered,
            "<path d='M 5.0,5.0 L 0.0,5.0 A 5.0,5.0 0 0 1 2.5,0.669872981078 Z'"
            " data-name='item 0' data-value='1'>"
            "<title>item 0 (1, 17%)</title></path>\n"
            "<path d='M 5.0,5.0 L 2.5,0.669872981078 A 5.0,5.0 0 0 1 10.0,5.0 Z'"
            " data-name='item 1' data-value='2'>"
            "<title>item 1 (2, 33%)</title></path>\n"
            "<path d='M 5.0,5.0 L 10.0,5.0 A 5.0,5.0 0 0 1 0.0,5.0 Z'"
            " data-name='item 2' data-value='3'>"
            "<title>item 2 (3, 50%)</title></path>\n"
        )

    def test_ctor(self):
        chart = charts.PieChart(charts.SvgRect(0, 0, 10, 20))
        self.assertEquals(chart.radius, 5)
        self.assertEquals(chart.center, charts.SvgPoint(5, 10))

        chart = charts.PieChart(charts.SvgRect(0, 0, 10, 20), 2)
        self.assertEquals(chart.radius, 3)
        self.assertEquals(chart.center, charts.SvgPoint(5, 10))

        chart = charts.PieChart(charts.SvgRect(0, 0, 10, 20), 2, 1)
        self.assertEquals(chart.radius, 1)
        self.assertEquals(chart.center, charts.SvgPoint(5, 10))

    def test_render_single_dataset(self):
        data = make_data_set()
        chart = charts.PieChart(charts.SvgRect(0, 0, 10, 10))
        rendered = chart.render(data)
        self.assertEqual(rendered.count("<path"), len(data))
        self._check_std_render(rendered)

    def test_render_single_data_matrix(self):
        chart = charts.PieChart(charts.SvgRect(0, 0, 10, 10))
        data = charts.DataMatrix(
            [charts.MetaData("record %s" % x) for x in xrange(2)],
            [charts.MetaData("item %s" % x) for x in xrange(3)],
            [[1, 1, 2], [0, 1, 1]]
        )
        self._check_std_render(chart.render(data))

    def test_render_rotated(self):
        data = make_data_set()
        chart = charts.PieChart(charts.SvgRect(0, 0, 10, 10), angle_start=math.pi)
        rendered = chart.render(data)
        self.assertEqual(rendered.count("<path"), len(data))
        self._check_rotated_render(rendered)

    def test_template_tag(self):
        data = make_data_set()
        template_tag = charts.PieChart.template_tag()
        self._check_std_render(template_tag(data, 10, 10))
        self._check_rotated_render(template_tag(data, 10, 10, angle_start=180))

    def test_render_single_item(self):
        chart = charts.PieChart(charts.SvgRect(0, 0, 10, 10))
        data = charts.DataSet([charts.DataPoint(1, "item 1")])
        rendered = chart.render(data)
        self.assertEqual(
            rendered.replace(".0", ""),
            "<circle cx='5' cy='5'"
            " data-name='item 1' data-value='1' r='5'>"
            "<title>item 1 (1, 100%)</title></circle>\n"
        )


class TestLineChartBase(SimpleTestCase):
    def test__value_point(self):
        chart = charts.LineChartBase(charts.SvgRect(0, 0, 40, 30), 0, False)
        self.assertEqual(chart._value_point(0, 1, 1), charts.SvgPoint(0, 30))
        self.assertEqual(chart._value_point(0.5, 1, 1), charts.SvgPoint(0, 15))
        self.assertEqual(chart._value_point(1, 1, 1), charts.SvgPoint(0, 0))
        self.assertEqual(chart._value_point(0.5, 1, 2), charts.SvgPoint(40, 15))
        self.assertEqual(chart._value_point(0.5, 1, 3), charts.SvgPoint(20, 15))

    def test_render_hgrid(self):
        chart = charts.LineChartBase(charts.SvgRect(0, 0, 40, 30), 0, False)
        self.assertEqual(
            chart.render_hgrid(3, {}).replace(".0", ""),
            "<path d='M 0,30 L 0,0 M 20,30 L 20,0 M 40,30 L 40,0'/>\n"
        )
        self.assertEqual(
            chart.render_hgrid(3, {"foo": "bar"}).replace(".0", ""),
            "<path d='M 0,30 L 0,0 M 20,30 L 20,0 M 40,30 L 40,0' foo='bar'/>\n"
        )

    def test__render_circle_offset(self):
        chart = charts.LineChartBase(charts.SvgRect(0, 0, 40, 30), 0, False)
        self.assertEqual(
            chart._render_circle_offset(
                charts.SvgPoint(2, 3),
                charts.DataPoint(34, "foo"),
                68,
                charts.RenderOptions(r=4)
            ),
            "<circle cx='2' cy='3' data-name='foo' data-value='34' r='4'>"
            "<title>foo (34)</title></circle>\n"
        )

    def test__render_circle(self):
        chart = charts.LineChartBase(charts.SvgRect(0, 0, 40, 30), 0, False)
        self.assertEqual(
            chart._render_circle(
                charts.DataPoint(34, "foo"),
                1, 3, 68,
                charts.RenderOptions(r=4)
            ).replace(".0", ""),
            "<circle cx='20' cy='15' data-name='foo' data-value='34' r='4'>"
            "<title>foo (34)</title></circle>\n"
        )


class TestLineChart(SimpleTestCase):
    points_str = (
        "<circle cx='0' cy='20' data-name='item 0' data-value='1' foo='1' r='4'>"
        "<title>item 0 (1)</title></circle>\n"
        "<circle cx='20' cy='10' data-name='item 1' data-value='2' foo='1' r='4'>"
        "<title>item 1 (2)</title></circle>\n"
        "<circle cx='40' cy='0' data-name='item 2' data-value='3' foo='1' r='4'>"
        "<title>item 2 (3)</title></circle>\n"
    )
    line_str = "<path d='M 0,20 L 20,10 40,0' foo='1'/>\n"
    grid_str = "<path class='grid' d='M 0,30 L 0,0 M 20,30 L 20,0 M 40,30 L 40,0'/>"
    trace_str = "<g foo='1'>%s%s</g>" % (line_str, points_str)
    maxDiff = None

    def test_points(self):
        chart = charts.LineChart(charts.SvgRect(0, 0, 40, 30))
        self.assertEqual(
            chart.points(make_data_set(), 3),
            [charts.SvgPoint(0, 20),
             charts.SvgPoint(20, 10),
             charts.SvgPoint(40, 0)]
        )

    def test_render_line(self):
        chart = charts.LineChart(charts.SvgRect(0, 0, 40, 30), 0)
        opts = charts.RenderOptions(foo=1, r=4)
        self.assertEqual(
            chart.render_line(make_data_set(), 3, opts).replace(".0", ""),
            self.line_str
        )
        self.assertEqual(
            chart.render_line(charts.DataSet([]), 3, opts).replace(".0", ""),
            "<path d='' foo='1'/>\n"
        )

    def test_render_points(self):
        chart = charts.LineChart(charts.SvgRect(0, 0, 40, 30), 0)
        opts = charts.RenderOptions(foo=1, r=4)
        self.assertEqual(
            chart.render_points(make_data_set(), 3, opts).replace(".0", ""),
            self.points_str
        )

    def test_render_data_trace(self):
        chart = charts.LineChart(charts.SvgRect(0, 0, 40, 30), 0)
        opts = charts.RenderOptions(foo=1, r=4)
        self.assertEqual(
            chart.render_data_trace(make_data_set(), 3, opts).replace(".0", ""),
            self.trace_str
        )

    def test_render(self):
        data = charts.DataMatrix(
            [charts.MetaData("item %s" % i) for i in xrange(3)],
            [charts.MetaData()] * 2,
            [[x + 1] * 2 for x in xrange(3)]
        )
        chart = charts.LineChart(charts.SvgRect(0, 0, 40, 30), 0)
        opts = charts.RenderOptions(foo=1, r=4)
        rendered = chart.render(data, options=opts).replace(".0", "")
        self.assertIn(self.grid_str, rendered)
        self.assertIn(self.trace_str, rendered)
        self.assertEqual(rendered.count(self.trace_str), 2)

    def test_render_single_dataset(self):
        chart = charts.LineChart(charts.SvgRect(0, 0, 40, 30), 0)
        opts = charts.RenderOptions(foo=1, r=4)
        rendered = chart.render(make_data_set(), options=opts).replace(".0", "")
        self.assertIn(self.grid_str, rendered)
        self.assertIn(self.trace_str, rendered)

    def test_template_tag(self):
        data = make_data_set()
        template_tag = charts.LineChart.template_tag()
        rendered = template_tag(data, 40, 30, attribute_foo=1, r=4).replace(".0", "")
        self.assertIn(self.grid_str, rendered)
        self.assertIn(self.trace_str, rendered)

    def test_chart_svg_template_tag(self):
        data = make_data_set()
        rendered = tt.chart_svg("line_chart", data, 40, 30, attribute_foo=1)
        self.assertMultiLineEqual(
            rendered.replace(".0", ""),
            """<svg class="line_chart"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    width="40" height="30" viewBox="0 0 40 30"
    >""" + self.grid_str + "\n" + self.trace_str + "</svg>"
        )


class TestStackedBarChart(SimpleTestCase):
    maxDiff = None

    def rendered_text(self, vals, x, scale, width, height, percents):
        h = [v * scale for v in vals]
        y = []
        cy = height
        for v in vals:
            cy -= v * scale
            y.append(cy)
        if percents:
            pc = [", %i%%" % round(float(v) / sum(vals) * 100) for v in vals]
        else:
            pc = [""] * len(vals)
        return (
            "<g>"
            "<rect data-name='item 0' data-value='{v[0]}' "
            "height='{h[0]}' width='{width}' x='{x}' y='{y[0]}'>"
            "<title>item 0 ({v[0]}{pc[0]})</title></rect>\n"
            "<rect data-name='item 1' data-value='{v[1]}' "
            "height='{h[1]}' width='{width}' x='{x}' y='{y[1]}'>"
            "<title>item 1 ({v[1]}{pc[1]})</title></rect>\n"
            "<rect data-name='item 2' data-value='{v[2]}' "
            "height='{h[2]}' width='{width}' x='{x}' y='{y[2]}'>"
            "<title>item 2 ({v[2]}{pc[2]})</title></rect>\n"
            "</g>\n".format(x=x, y=y, h=h, v=vals, width=width, pc=pc)
        ).replace(".0", "")

    def test_render_bar(self):
        chart = charts.StackedBarChart(charts.SvgRect(0, 0, 40, 12), 0)
        self.assertMultiLineEqual(
            chart.render_bar(make_data_set(), 12).replace(".0", ""),
            self.rendered_text([1, 2, 3], 0, 1, 40, 12, False)
        )

    def test_render_bar_normalized(self):
        chart = charts.StackedBarChart(charts.SvgRect(0, 0, 40, 12), 0, True)
        self.assertMultiLineEqual(
            chart.render_bar(make_data_set(), 12).replace(".0", ""),
            self.rendered_text([1, 2, 3], 0, 2, 40, 12, True)
        )

    def test_render_bar_rubrect(self):
        chart = charts.StackedBarChart(charts.SvgRect(0, 0, 40, 12), 0)
        self.assertMultiLineEqual(
            chart.render_bar(make_data_set(), 12, charts.SvgRect(20, 0, 20, 6))
            .replace(".0", ""),
            self.rendered_text([1, 2, 3], 20, 0.5, 20, 6, False)
        )

    def test_render_bar_rubrect_normalized(self):
        chart = charts.StackedBarChart(charts.SvgRect(0, 0, 40, 12), 0, True)
        self.assertMultiLineEqual(
            chart.render_bar(make_data_set(), 12, charts.SvgRect(20, 0, 20, 6))
            .replace(".0", ""),
            self.rendered_text([1, 2, 3], 20, 1, 20, 6, True)
        )

    def test_subrect_sep0(self):
        chart = charts.StackedBarChart(charts.SvgRect(0, 0, 40, 12), separation=0)
        self.assertEqual(chart.subrect(2, 0), chart.rect)
        self.assertEqual(chart.subrect(0, 4), charts.SvgRect(0, 0, 10, 12))
        self.assertEqual(chart.subrect(1, 4), charts.SvgRect(10, 0, 10, 12))
        self.assertEqual(chart.subrect(2, 4), charts.SvgRect(20, 0, 10, 12))
        self.assertEqual(chart.subrect(3, 4), charts.SvgRect(30, 0, 10, 12))

    def test_subrect_sep1(self):
        chart = charts.StackedBarChart(charts.SvgRect(0, 0, 40, 12), separation=1)
        self.assertEqual(chart.subrect(2, 0), chart.rect)
        self.assertEqual(chart.subrect(0, 2), charts.SvgRect(5, 0, 10, 12))
        self.assertEqual(chart.subrect(1, 2), charts.SvgRect(25, 0, 10, 12))

    def test_subrect_sep05(self):
        chart = charts.StackedBarChart(charts.SvgRect(0, 0, 45, 12), separation=.5)
        self.assertEqual(chart.subrect(2, 0), chart.rect)
        self.assertEqual(chart.subrect(0, 3), charts.SvgRect(2.5, 0, 10, 12))
        self.assertEqual(chart.subrect(1, 3), charts.SvgRect(17.5, 0, 10, 12))
        self.assertEqual(chart.subrect(2, 3), charts.SvgRect(32.5, 0, 10, 12))

    def test_render(self):
        data = charts.DataMatrix(
            [charts.MetaData("row %s" % i) for i in xrange(4)],
            [charts.MetaData("item %s" % i) for i in xrange(3)],
            [[1, 2, 3], [3, 2, 1], [6, 6, 0], [12, 0, 0]]
        )
        chart = charts.StackedBarChart(charts.SvgRect(0, 0, 40, 12), separation=0)
        rendered = chart.render(data).replace(".0", "")
        self.assertIn(
            self.rendered_text([1, 2, 3], 0, 1, 10, 12, False),
            rendered
        )
        self.assertIn(
            self.rendered_text([3, 2, 1], 10, 1, 10, 12, False),
            rendered
        )

    def test_render_normalized(self):
        data = charts.DataMatrix(
            [charts.MetaData("row %s" % i) for i in xrange(4)],
            [charts.MetaData("item %s" % i) for i in xrange(3)],
            [[1, 2, 3], [3, 2, 1], [6, 6, 0], [12, 0, 0]]
        )
        chart = charts.StackedBarChart(
            charts.SvgRect(0, 0, 40, 12),
            normalized=True,
            separation=0
        )
        rendered = chart.render(data).replace(".0", "")
        self.assertIn(
            self.rendered_text([1, 2, 3], 0, 2, 10, 12, True),
            rendered
        )
        self.assertIn(
            self.rendered_text([3, 2, 1], 10, 2, 10, 12, True),
            rendered
        )

    def test_template_tag(self):
        data = make_data_set()
        data = charts.DataMatrix(
            [charts.MetaData("row %s" % i) for i in xrange(4)],
            [charts.MetaData("item %s" % i) for i in xrange(3)],
            [[1, 2, 3], [3, 2, 1], [6, 6, 0], [12, 0, 0]]
        )
        template_tag = charts.StackedBarChart.template_tag()
        rendered = template_tag(data, 40, 12, separation=0).replace(".0", "")
        self.assertIn(
            self.rendered_text([1, 2, 3], 0, 1, 10, 12, False),
            rendered
        )
        self.assertIn(
            self.rendered_text([3, 2, 1], 10, 1, 10, 12, False),
            rendered
        )


class TestStackedLineChart(SimpleTestCase):
    maxDiff = None

    points_str0 = (
        "<g data-item='item 0' foo='1'>"
        "<circle cx='0' cy='50' data-name='rec 0' data-value='1' foo='1' r='4'>"
        "<title>rec 0 (1)</title></circle>\n"
        "<circle cx='20' cy='40' data-name='rec 1' data-value='2' foo='1' r='4'>"
        "<title>rec 1 (2)</title></circle>\n"
        "<circle cx='40' cy='30' data-name='rec 2' data-value='3' foo='1' r='4'>"
        "<title>rec 2 (3)</title></circle>\n"
        "</g>\n"
    )
    points_str1 = (
        "<g data-item='item 1' foo='1'>"
        "<circle cx='0' cy='40' data-name='rec 0' data-value='1' foo='1' r='4'>"
        "<title>rec 0 (1)</title></circle>\n"
        "<circle cx='20' cy='0' data-name='rec 1' data-value='4' foo='1' r='4'>"
        "<title>rec 1 (4)</title></circle>\n"
        "</g>\n"
    )
    line_str0 = (
        "<path d='M 0,60 L 0,50 20,40 40,30 40,60 20,60 0,60' foo='1'>"
        "<title>item 0</title></path>\n"
    )
    line_str1 = (
        "<path d='M 0,50 L 0,40 20,0 40,30 40,30 20,40 0,50' foo='1'>"
        "<title>item 1</title></path>\n"
    )
    grid_str = "<path class='grid' d='M 0,60 L 0,0 M 20,60 L 20,0 M 40,60 L 40,0'/>\n"
    points_str0_norm = (
        "<g data-item='item 0' foo='1'>"
        "<circle cx='0' cy='30' data-name='rec 0' data-value='1' foo='1' r='4'>"
        "<title>rec 0 (1, 50%)</title></circle>\n"
        "<circle cx='20' cy='40' data-name='rec 1' data-value='2' foo='1' r='4'>"
        "<title>rec 1 (2, 33%)</title></circle>\n"
        "<circle cx='40' cy='0' data-name='rec 2' data-value='3' foo='1' r='4'>"
        "<title>rec 2 (3, 100%)</title></circle>\n"
        "</g>\n"
    )
    points_str1_norm = (
        "<g data-item='item 1' foo='1'>"
        "<circle cx='0' cy='0' data-name='rec 0' data-value='1' foo='1' r='4'>"
        "<title>rec 0 (1, 50%)</title></circle>\n"
        "<circle cx='20' cy='0' data-name='rec 1' data-value='4' foo='1' r='4'>"
        "<title>rec 1 (4, 67%)</title></circle>\n"
        "</g>\n"
    )
    line_str0_norm = (
        "<path d='M 0,60 L 0,30 20,40 40,0 40,60 20,60 0,60' foo='1'>"
        "<title>item 0</title></path>\n"
    )
    line_str1_norm = (
        "<path d='M 0,30 L 0,0 20,0 40,0 40,0 20,40 0,30' foo='1'>"
        "<title>item 1</title></path>\n"
    )
    points_str0_nd = (
        "<g data-item='item 0' foo='1'>"
        "<circle cx='0' cy='30' data-name='rec 0' data-value='1' foo='1' r='4'>"
        "<title>rec 0 (1, 50%)</title></circle>\n"
        "<circle cx='20' cy='40' data-name='rec 1' data-value='2' foo='1' r='4'>"
        "<title>rec 1 (2, 33%)</title></circle>\n"
        "<circle cx='40' cy='60' data-name='rec 2' data-value='0' foo='1' r='4'>"
        "<title>rec 2 (0, 100%)</title></circle>\n"
        "</g>\n"
    )
    points_str1_nd = (
        "<g data-item='item 1' foo='1'>"
        "<circle cx='0' cy='0' data-name='rec 0' data-value='1' foo='1' r='4'>"
        "<title>rec 0 (1, 50%)</title></circle>\n"
        "<circle cx='20' cy='0' data-name='rec 1' data-value='4' foo='1' r='4'>"
        "<title>rec 1 (4, 67%)</title></circle>\n"
        "</g>\n"
    )
    line_str0_nd = (
        "<path d='M 0,60 L 0,30 20,40 40,60 40,60 20,60 0,60' foo='1'>"
        "<title>item 0</title></path>\n"
    )
    line_str1_nd = (
        "<path d='M 0,30 L 0,0 20,0 40,60 40,60 20,40 0,30' foo='1'>"
        "<title>item 1</title></path>\n"
    )

    def test_render(self):
        data = charts.DataMatrix(
            [charts.MetaData("rec %s" % i) for i in xrange(3)],
            [charts.MetaData("item %s" % i) for i in xrange(2)],
            [[1, 1], [2, 4], [3, 0]]
        )
        chart = charts.StackedLineChart(charts.SvgRect(0, 0, 40, 60))
        opts = charts.RenderOptions(foo=1, r=4)
        self.assertMultiLineEqual(
            chart.render(data, opts).replace(".0", ""),
            self.line_str1 + self.line_str0 +
            self.grid_str +
            self.points_str1 + self.points_str0
        )

    def test_render_nodata_normalized(self):
        data = charts.DataMatrix(
            [charts.MetaData("rec %s" % i) for i in xrange(3)],
            [charts.MetaData("item %s" % i) for i in xrange(2)],
            [[1, 1], [2, 4], [0, 0]]
        )
        chart = charts.StackedLineChart(charts.SvgRect(0, 0, 40, 60), 0, True)
        opts = charts.RenderOptions(foo=1, r=4)
        self.assertMultiLineEqual(
            chart.render(data, opts).replace(".0", ""),
            self.line_str1_nd + self.line_str0_nd +
            self.grid_str +
            self.points_str1_nd + self.points_str0_nd
        )

    def test_render_normalized(self):
        data = charts.DataMatrix(
            [charts.MetaData("rec %s" % i) for i in xrange(3)],
            [charts.MetaData("item %s" % i) for i in xrange(2)],
            [[1, 1], [2, 4], [3, 0]]
        )
        chart = charts.StackedLineChart(charts.SvgRect(0, 0, 40, 60), 0, True)
        opts = charts.RenderOptions(foo=1, r=4)
        self.assertMultiLineEqual(
            chart.render(data, opts).replace(".0", ""),
            self.line_str1_norm + self.line_str0_norm +
            self.grid_str +
            self.points_str1_norm + self.points_str0_norm
        )

    def test_template_tag(self):
        data = charts.DataMatrix(
            [charts.MetaData("rec %s" % i) for i in xrange(3)],
            [charts.MetaData("item %s" % i) for i in xrange(2)],
            [[1, 1], [2, 4], [3, 0]]
        )
        template_tag = charts.StackedLineChart.template_tag()
        rendered = template_tag(data, 40, 60, normalized=True, attribute_foo=1, r=4)
        self.assertMultiLineEqual(
            rendered.replace(".0", ""),
            self.line_str1_norm + self.line_str0_norm +
            self.grid_str +
            self.points_str1_norm + self.points_str0_norm
        )
