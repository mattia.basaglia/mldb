"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import math
from django.utils.html import escape
from django.utils.safestring import mark_safe
from ..simple_page.templatetags.simple_page import make_attrs


class MetaData(object):
    """
    Extra information associated with chart data
    """
    def __init__(self, label="", id="", link=None, extra=None):
        self.label = label
        self.id = id
        self.link = link
        self.extra = extra

    def ctor_args(self):
        """
        Returns a tuple of arguments that can be passed to the constructor
        """
        return (self.label, self.id, self.link, self.extra)

    def wrap_link(self, svg, xmlns="xlink", request=None):
        """
        Wraps the given SVG snippet into a link
        """
        if not self.link:
            return svg

        if xmlns and xmlns[-1] != ':':
            xmlns += ':'

        if request:
            link = request.build_absolute_uri(self.link)
        else:
            link = self.link

        return "<a %shref='%s'>%s</a>" % (
            xmlns,
            escape(link),
            svg,
        )


class DataPoint(MetaData):
    """
    A data point in the graph
    """
    def __init__(self, value, *args, **kwargs):
        """
        \param value Data value
        All other arguments are forwarded to MetaData
        """
        MetaData.__init__(self, *args, **kwargs)
        self.value = value

    def normalized(self, max):
        """
        Percentage of the maximum
        """
        return float(self.value) / max if max else 1.0


class DataSet(MetaData):
    """
    A list of data points
    """
    def __init__(self, points, *args, **kwargs):
        """
        \param points List of DataPoints
        All other arguments are forwarded to MetaData
        """
        MetaData.__init__(self, *args, **kwargs)
        self.data = list(points)

    def max_value(self):
        """
        Computes the maximum value across all data points
        """
        return max(point.value for point in self.data) if self.data else 0

    def total(self):
        """
        Computes the sum of all data points
        """
        return sum(point.value for point in self.data)

    def append(self, point):
        """
        Appends a data point
        """
        self.data.append(point)

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        return iter(self.data)

    def __getitem__(self, key):
        return self.data[key]


class DataMatrix(object):
    """
    Collects a two-dimensional set of data
    """
    def __init__(self, records, items, values):
        """
        \param records     List of MetaData
        \param items  List of MetaData
        \param values   Matrix of values
        \pre len(records) == len(values) and all(len(records) == len items for record in values)
        """
        self.records = records
        self.items = items
        self.values = values

    def value(self, record_id, item_id):
        """
        Returns a raw value at the given record/item
        """
        return self.values[record_id][item_id]

    @property
    def range_items(self):
        """
        Returns a range of item indices
        """
        return xrange(len(self.items))

    @property
    def range_records(self):
        """
        Returns a range of record indices
        """
        return xrange(len(self.records))

    def record_datasets(self):
        """
        Generator yielding all record datasets
        """
        for id in self.range_records:
            yield self.record_dataset(id)

    def item_datasets(self):
        """
        Generator yielding all item datasets
        """
        for id in self.range_items:
            yield self.item_dataset(id)

    def record_dataset(self, index):
        """
        Returns the DataSet corresponding to the given record
        """
        record = self.records[index]
        return DataSet(
            [
                DataPoint(
                    self.value(index, item_id),
                    *self.items[item_id].ctor_args()
                )
                for item_id in self.range_items
            ],
            *record.ctor_args()
        )

    def item_dataset(self, index):
        """
        Returns the DataSet corresponding to the given item
        """
        item = self.items[index]
        return DataSet(
            [
                DataPoint(
                    self.value(record_id, index),
                    *self.records[record_id].ctor_args()
                )
                for record_id in self.range_records
            ],
            *item.ctor_args()
        )

    def max_value(self):
        """
        Returns the maximum among all values in the matrix
        """
        return float(max(map(max, self.values)))

    @property
    def transposed(self):
        """
        Returns a view to the same underlying data that swaps items and records
        """
        return TransposedMatrix(self.records, self.items, self.values)


class TransposedMatrix(DataMatrix):
    """
    DataMatrix swapping items and records
    """
    def __init__(self, records, items, values):
        self.records = items
        self.items = records
        self.values = values

    def value(self, record_id, item_id):
        return super(TransposedMatrix, self).value(item_id, record_id)

    @property
    def transposed(self):
        return DataMatrix(self.items, self.records, self.values)


class MatrixViewSingleRecord(DataMatrix):
    """
    This class maps a single DataSet as a record
    """

    def __init__(self, data_set):
        self.data_set = data_set
        self.records = [self.data_set]
        self.items = self.data_set

    def value(self, record_id, item_id):
        return self.data_set[item_id].value

    def max_value(self):
        return self.data_set.max_value()

    def record_dataset(self, index):
        return self.data_set

    @property
    def transposed(self):
        return MatrixViewSingleItem(self.data_set)


class MatrixViewSingleItem(DataMatrix):
    """
    This class maps a single DataSet as an item
    """
    def __init__(self, data_set):
        self.data_set = data_set
        self.records = self.data_set
        self.items = [self.data_set]

    def value(self, record_id, item_id):
        return self.data_set[record_id].value

    def max_value(self):
        return self.data_set.max_value()

    def item_dataset(self, index):
        return self.data_set

    @property
    def transposed(self):
        return MatrixViewSingleRecord(self.data_set)


class SvgPoint(object):
    """
    SVG coordinates in user units
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "%s,%s" % (self.x, self.y)

    def __repr__(self):
        return "SvgPoint" + repr((self.x, self.y))

    def __eq__(self, oth):
        return self._as_tuple() == oth._as_tuple()

    def __ne__(self, oth):
        return not (self == oth)

    def _as_tuple(self):
        return (self.x, self.y)


class SvgRect(object):
    """
    SVG axis-aligned rectangle in user units
    """
    def __init__(self, x=0, y=0, width=0, height=0):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    @property
    def top_left(self):
        return SvgPoint(self.x, self.y)

    @property
    def top_right(self):
        return SvgPoint(self.x + self.width, self.y)

    @property
    def bottom_right(self):
        return SvgPoint(self.x + self.width, self.y + self.height)

    @property
    def bottom_left(self):
        return SvgPoint(self.x, self.y + self.height)

    @property
    def center(self):
        return SvgPoint(
            self.x + self.width / 2.0,
            self.y + self.height / 2.0
        )

    def __repr__(self):
        return "SvgRect(%.2gx%.2g+%.2g+%.2g)" % \
            (self.width, self.height, self.x, self.y)

    def __eq__(self, oth):
        return self._as_tuple() == oth._as_tuple()

    def __ne__(self, oth):
        return not (self == oth)

    def _as_tuple(self):
        return (self.x, self.y, self.width, self.height)


class TraitsItem(object):
    """
    An item for ChartTraits, represent an available parameter
    """
    def __init__(self, parameter, type, default, name=None):
        # Function parameter name
        self.parameter = parameter
        # Type/callable to pass values to
        self.type = type
        # Default value (does not pass through self.type)
        self.default = default
        # Key looked for in the input dictionary
        self.name = name if name is not None else parameter

    def get(self, value_dict):
        """
        Returns the value of this item based on the given dictionary
        """
        if self.type is bool:
            return self.name in value_dict
        if self.name in value_dict:
            return self.type(value_dict[self.name])
        return self.default

    def update_kwargs(self, kwargs, value_dict):
        """
        Updates a dict usable as **kwargs to the item target
        """
        kwargs[self.parameter] = self.get(value_dict)


class ChartTraits(object):
    """
    Describes what options and parameters are available for a chart class
    """
    default_options_args = {"class_prefix", "id_prefix", "request"}
    options_extra_prefix = "attribute_"

    def __init__(self, extra_ctor, extra_options=[]):
        self.extra_ctor = extra_ctor
        self.extra_options = extra_options

    def get_kwargs(self, description, value_dict):
        """
        Returns a dict usable as **kwargs for the proper target
        """
        kwargs = {}
        for item in description:
            item.update_kwargs(kwargs, value_dict)
        return kwargs

    def get_options_kwargs(self, value_dict):
        """
        Returns a dict usable as **kwargs for RenderOptions.__init__
        """
        kwargs = self.get_kwargs(self.extra_options, value_dict)
        for k, v in value_dict.iteritems():
            if k not in kwargs:
                if k in self.default_options_args:
                    kwargs[k] = v
                elif k.startswith(self.options_extra_prefix):
                    kwargs[k[len(self.options_extra_prefix):]] = v
        return kwargs

    def get_ctor_kwargs(self, value_dict):
        """
        Returns a dict usable as **kwargs for the chart class __init__
        """
        return self.get_kwargs(self.extra_ctor, value_dict)


class ChartBase(object):
    """
    Base class for graphs
    """
    traits = ChartTraits([TraitsItem("normalized", bool, True)])

    def __init__(self, rect, padding, normalized):
        """
        \param rect         SvgRect for the bounding box of the rendered data
        \param normalized   Whether the total of a record is to be considered as 100%
        \param padding      Extra space to leave around \p rect
        """
        self.rect = rect
        self.normalized = normalized
        self.rect = SvgRect(
            self.rect.x + padding,
            self.rect.y + padding,
            self.rect.width - padding * 2,
            self.rect.height - padding * 2,
        )

    def format_title(self, point, total):
        """
        Returns a suitable title fot the given data point
        \param point A DataPoint for the value to display
        \param total Used to display a percentage (Only if self.normalized)
        """
        if not self.normalized:
            return "%s (%s)" % (point.label, point.value)
        return "{} ({}, {:.0%})".format(point.label, point.value, point.normalized(total))

    def point_rel2abs(self, point):
        """
        Converts a relative/normalized point into an absolute point
        """
        return SvgPoint(
            round(self.rect.x + self.rect.width * point.x, 7),
            round(self.rect.y + self.rect.height * (1 - point.y), 7),
        )

    def rect_rel2abs(self, rect):
        """
        Converts a relative/normalized rect into an absolute point
        """
        height = rect.height * self.rect.height
        pos = self.point_rel2abs(rect.top_left)
        return SvgRect(
            pos.x,
            pos.y - height,
            rect.width * self.rect.width,
            height
        )

    @classmethod
    def template_tag(cls):
        """
        Returns a function usable as a template tag
        """
        def func(data, width, height, padding=0, **kwargs):
            rect = SvgRect(0, 0, float(width), float(height))

            options = RenderOptions(
                **cls.traits.get_options_kwargs(kwargs)
            )

            ctor_kwargs = cls.traits.get_ctor_kwargs(kwargs)

            return mark_safe(
                cls(rect, float(padding), **ctor_kwargs)
                .render(data, options=options)
            )
        return func


class RenderOptions(object):
    """
    Common rendering options for charts
    """
    def __init__(self, class_prefix=None, id_prefix=None, request=None, **kwargs):
        """
        \param class_prefix Used to generate CSS classes from metadata id
        \param id_prefix    Used to generate XML IDs from metadata id
        \param request      Django HTTP request object to create absolute uris
        \param kwargs       Extra attributes
        """
        self.class_prefix = class_prefix
        self.attrs = kwargs
        self.id_prefix = id_prefix
        self.request = request

    def attribute_string(self, metadata, **kwargs):
        """
        Returns a string representing attributes for the given metasata
        """
        return make_attrs(self.attributes(metadata, **kwargs))

    def attributes(self, metadata, **kwargs):
        """
        Returns a dict of attributes
        \param metadata Metadata used to extract some values
        \param kwargs   Used to override attributes, set to None to remove them
        """
        attrs = self.attrs.copy()

        if isinstance(metadata, DataPoint):
            attrs["data-value"] = metadata.value
            attrs["data-name"] = metadata.label

        if metadata.id:
            if self.class_prefix is not None:
                attrs["class"] = self.class_prefix + metadata.id
            if self.id_prefix is not None:
                attrs["id"] = self.id_prefix + metadata.id

        attrs.update(kwargs)

        return {k: v for k, v in attrs.iteritems() if v is not None}

    def render_element(self, element, metadata, title=None, **kwargs):
        """
        Renders a full SVG element.
        \param element  Element tag name
        \param metadata MetaData
        \param title    If None, metadata.label will be used
        \param kwargs   Attribute override

        It wraps the element into the metadata link, and adds title
        and attributes as needed.
        """
        if title is None and metadata.label:
            title = metadata.label

        if title:
            end = "><title>%s</title></%s>" % (escape(title), element)
        else:
            end = "/>"
        attributes = self.attribute_string(metadata, **kwargs)
        return metadata.wrap_link(
            "<%s %s%s" % (element, attributes, end),
            request=self.request
        ) + "\n"


class PieChart(ChartBase):
    """
    Pie chart
    """
    traits = ChartTraits([
        TraitsItem("angle_start", lambda s: -float(s) / 180 * math.pi, 0)
    ])

    def __init__(self, rect, padding=0, radius=None, angle_start=0):
        """
        \param radius        Radius of the chart (in svg user units)
        \param center        A SvgPoint
        \param angle_start   Starting angle (in radians)
        """
        super(PieChart, self).__init__(rect, padding, True)
        self.radius = radius if radius else min(self.rect.width, self.rect.height) / 2.0
        self.center = self.rect.center
        self.angle_start = angle_start

    def _circle_point(self, angle):
        """
        Returns a coordinate tuple of a point around the pie edge
        """
        return SvgPoint(
            self.center.x + self.radius * math.cos(angle),
            self.center.y + self.radius * math.sin(angle)
        )

    def render(self, data, options=RenderOptions()):
        """
        Renders the given data as SVG paths
        \param data     A DataSet object
        \param options  Options for the chart elements
        """
        if isinstance(data, DataSet):
            data = MatrixViewSingleRecord(data)
        slices = ""
        angle = self.angle_start
        data_sets = list(data.item_datasets())
        if len(data_sets) == 1:
            item = data_sets[0]
            point = DataPoint(item.total(), *item.ctor_args())
            title = self.format_title(point, point.value)
            return options.render_element("circle", point, title,
                r=self.radius, cx=self.center.x, cy=self.center.y
            )

        total = sum(item.total() for item in data_sets)
        for item in data_sets:
            point = DataPoint(item.total(), *item.ctor_args())
            angle_delta = math.pi * 2 * point.normalized(total)
            title = self.format_title(point, total)
            slice_path = "M %s L %s A %s 0 %s 1 %s Z" % (
                self.center,
                self._circle_point(angle),
                SvgPoint(self.radius, self.radius),
                1 if angle_delta > math.pi else 0,
                self._circle_point(angle + angle_delta),
            )
            slices += options.render_element("path", point, title, d=slice_path)
            angle += angle_delta
        return slices


class LineChartBase(ChartBase):
    """
    Common functionality for line charts
    """
    def _value_point(self, percent, index, size):
        """
        Returns an absolute point based on a data value
        \param percent  Normalized height of the point
        \param index    Index of the record
        \param size     Number of records
        """
        return self.point_rel2abs(SvgPoint(
            0 if size < 2 else float(index) / (size - 1),
            percent
        ))

    def render_hgrid(self, steps, attrs):
        """
        Renders a horizontal grid
        \param steps    Number of lines to draw
        \param attrs    Attibutes to pass to the SVG path
        """
        attrs["d"] = " ".join(
            "M %s L %s" % (
                self._value_point(0, i, steps),
                self._value_point(1, i, steps),
            )
            for i in range(steps)
        )
        return "<path %s/>\n" % make_attrs(attrs)

    def _render_circle(self, data_point, index, size, max, options):
        """
        Renders a SVG circle for a data point
        \param data_point   Data point for value and metadata
        \param index        Index of \p data_point in the data set
        \param size         Number of elements in the data set
        \param max          Maximum value
        \param options      Options for the chart element
        """
        pos = self._value_point(data_point.normalized(max), index, size)
        return self._render_circle_offset(pos, data_point, max, options)

    def _render_circle_offset(self, pos, data_point, max, options):
        """
        Renders a SVG circle for a data point
        \param pos          Point position (center)
        \param data_point   Data point for value and metadata
        \param max          Maximum value
        \param options      Options for the chart element
        """
        title = self.format_title(data_point, max)
        return options.render_element("circle", data_point, title, cx=pos.x, cy=pos.y)


class LineChart(LineChartBase):
    """
    Line chart

    Each item in the data becomes a line.
    All data points have a y position relative to the maximum element.
    Records become points on the lines.
    """
    traits = ChartTraits([], [TraitsItem("r", float, 4, "radius")])

    def __init__(self, rect, padding=0):
        super(LineChart, self).__init__(rect, padding, False)

    def points(self, data_set, max):
        """
        Evaluates positions for the DataSet
        \param data_set DataSet for the item to represent
        \param max      Value to be considered as maximum
        """
        return [
            self._value_point(data_point.normalized(max), index, len(data_set))
            for index, data_point in enumerate(data_set)
        ]

    def render_line(self, data_set, max, options=RenderOptions()):
        """
        Renders a path representing the data set
        \param data_set     DataSet for the item to represent
        \param max          Value to be considered as maximum
        \param options      Options for the path element
        """
        points = self.points(data_set, max)
        if points:
            path_d = "M " + str(points[0]) \
                     + " L " + " ".join(map(str, points[1:]))
        else:
            path_d = ""
        return options.render_element("path", data_set, d=path_d, r=None)

    def render_points(self, data_set, max, options=RenderOptions()):
        """
        Renders circle elements for the data set
        \param data_set     DataSet for the item to represent
        \param max          Value to be considered as maximum
        \param options      Options for the chart elements
        """
        return "".join(
            self._render_circle(point, index, len(data_set), max, options)
            for index, point in enumerate(data_set)
        )

    def render_data_trace(self, data_set, max, options=RenderOptions()):
        """
        Renders both line and points for a single item
        \param data_set     DataSet for the item to represent
        \param max          Value to be considered as maximum
        \param options      Options for the chart elements
        """
        return "<g %s>%s%s</g>" % (
            options.attribute_string(data_set, r=None),
            self.render_line(data_set, max, options),
            self.render_points(data_set, max, options),
        )

    def render(self, data, grid_class="grid", options=RenderOptions()):
        """
        Renders the whole graph
        \param data         A matrix view or a single DataSet
        \param grid_class   CSS class for the grid path
        \param options      Options for the chart elements
        """
        if isinstance(data, DataSet):
            data = MatrixViewSingleItem(data)

        svg = self.render_hgrid(len(data.records), {"class": grid_class})
        global_max = data.max_value()
        for data_set in reversed(list(data.item_datasets())):
            svg += self.render_data_trace(data_set, global_max, options)
        return svg


class StackedBarChart(ChartBase):
    """
    Stacked bar chart

    Each record is a stack of item bars
    """
    traits = ChartTraits([
        TraitsItem("normalized", bool, False),
        TraitsItem("separation", float, 1.0),
    ])

    def __init__(self, rect, padding=0, normalized=False, separation=1):
        super(StackedBarChart, self).__init__(rect, padding, normalized)
        self.separation = separation

    def render_bar(self, data_set, max, sub_rect=None, options=RenderOptions()):
        """
        Renders a stacked bar for the given data as SVG paths
        \param data_set     A DataSet object
        \param max          Maximum value for a dataset sum
        \param sub_rect     Rect to fill
        \param options      Options for the chart elements
        """
        items = ""

        if not sub_rect:
            sub_rect = self.rect

        y = sub_rect.y + sub_rect.height

        if self.normalized:
            max = data_set.total()

        for point in data_set:
            abs_rect = SvgRect(
                x=sub_rect.x,
                y=y,
                width=sub_rect.width,
                height=float(point.value) / max * sub_rect.height
            )
            abs_rect.y -= abs_rect.height
            title = self.format_title(point, max)

            items += options.render_element(
                "rect", point, title,
                x=abs_rect.x, y=abs_rect.y,
                width=abs_rect.width, height=abs_rect.height
            )
            y -= abs_rect.height
        return "<g>%s</g>\n" % items

    def subrect(self, index, size):
        """
        Gets a relative rectangle for the record at the given index
        \param index Record index
        \param size  Number of records
        """
        if size == 0:
            return self.rect
        width = 1.0 / (size + size * self.separation)
        gap_with = width * self.separation
        x = gap_with / 2 + (gap_with + width) * index
        return self.rect_rel2abs(SvgRect(x, 0.0, width, 1.0))

    def render(self, data, options=RenderOptions()):
        """
        Renders the data matrix with the given options
        """
        bars = ""
        global_max = max(ds.total() for ds in data.record_datasets())
        for index, data_set in enumerate(data.record_datasets()):
            subrect = self.subrect(index, len(data.records))
            bars += self.render_bar(data_set, global_max, subrect, options)
        return bars


class StackedLineChart(LineChartBase):
    traits = ChartTraits(
        [TraitsItem("normalized", bool, True)],
        [TraitsItem("r", float, 4, "radius")]
    )

    def __init__(self, rect, padding=0, normalized=False):
        super(StackedLineChart, self).__init__(rect, padding, normalized)

    # TODO Clean up this ugly function
    def render(self, data, options=RenderOptions()):
        accumulate = [[0] * len(data.items) for i in data.range_records]
        local_max = []
        for r_id in data.range_records:
            total = 0.0
            for it_id in data.range_items:
                accumulate[r_id][it_id] = total
                total += data.value(r_id, it_id)
            local_max.append(total)
        global_max = max(local_max) if local_max else 0.0

        def max_for(record_id):
            return local_max[record_id] if self.normalized else global_max

        def normalize(value, record_id):
            max_value = max_for(record_id)
            if not max_value:
                return 0
            return value / max_value

        svg_paths = ""
        svg_points = ""

        for it_id, item in reversed(list(enumerate(data.items))):
            start = self._value_point(normalize(accumulate[0][it_id], 0), 0, len(data.records))
            path = "M %s L " % start
            circles = ""
            for r_id, record in enumerate(data.record_datasets()):
                value = data.value(r_id, it_id)
                pos_y = normalize(value + accumulate[r_id][it_id], r_id)
                pos = self._value_point(pos_y, r_id, len(data.records))
                path += str(pos) + " "
                if value or it_id == 0:
                    data_point = DataPoint(value, *record.ctor_args())
                    data_point.id = None
                    circles += self._render_circle_offset(
                        pos,
                        data_point,
                        max_for(r_id),
                        options
                    )
            for r_id in reversed(data.range_records):
                pos_y = normalize(accumulate[r_id][it_id], r_id)
                pos = self._value_point(pos_y, r_id, len(data.records))
                path += str(pos) + " "

            if circles:
                svg_points += "<g data-item='%s' %s>%s</g>\n" % (
                    item.label,
                    options.attribute_string(item, r=None),
                    circles
                )

            svg_paths += options.render_element(
                "path",
                item,
                d=path.rstrip(" "),
                r=None
            )

        return (
            svg_paths +
            self.render_hgrid(len(data.records), {"class": "grid"}) +
            svg_points
        )


abstract = [ChartBase, LineChartBase]
