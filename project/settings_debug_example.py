"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import absolute_import
# Load default settings
from .settings_base import *

# This should be something unique, you can generate one with
# echo 'from django.core.management.utils import *; print get_random_secret_key()' | python
SECRET_KEY = 'Your random string'

# Enables debug info display
DEBUG = True

# Simple sqlite3 database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, "data", "db.sqlite3"),
    }
}

# Whoosh search backend (You'll need to install Whoosh)
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, "data", "whoosh_index"),
    },
}

# Dummy cache backend
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
# If you want to actually have caching enabled, you can use
#    'default': {
#        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
#        'LOCATION': 'django_cache',
#    }
# And call manage.py createcachetable


# If you want to enable the Django debug toolbar
# (First install django-debug-toolbar)
# INSTALLED_APPS += [
#     'debug_toolbar',
# ]
# MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')
# INTERNAL_IPS = ['127.0.0.1']
# DEBUG_TOOLBAR_PATCH_SETTINGS = False
# ROOT_URLCONF = "project.debug_urls"
