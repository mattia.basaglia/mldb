MLDB
====

This is a Django-based website that provides an interface to search and find
statistics from MLP transcripts.


License
-------

AGPLv3+, see COPYING


Installation
------------

First, ensure the requirements listed in scripts/requirements.pip are satisfied
(you can use `pip install -r scripts/requirements.pip` for this).
If you use `virtualenv`, you can create an environment called `env` and
`manage.py` will detect it automatically. This step can be achieved by calling
`scripts/environment.sh`.

You'll need to create `project/settings.py` with installation-specific settings.
You can have a look at `project/settings_debug_example.py` for some settings
that are easy to set up for a development server (you'll need to install
additional packages for it to work).


Running
-------

A development server can be started with `scripts/manage.sh runserver`, a
WSGI script is provided in `project/wsgi.py`.
Please refer to the Django and your web server documentation for more details.


Sources
-------

See https://gitlab.com/mattia.basaglia/mldb


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>
